@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">Stock</div>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- Button trigger modal -->
                            <!-- Modal -->
                            <div class="card-body">
                                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1"
                                               class="table table-bordered table-striped dataTable dtr-inline"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Serial
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Product
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Browser: activate to sort column ascending">Quantity
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Status
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($stocks as $key=>$stock)
                                                <tr>
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{optional($stock->product)->title}}</td>
                                                    <td>{{$stock->quantity}}</td>
                                                    <td>
                                                        @if($stock->status==1)
                                                            <span class="badge badge-pill badge-success">Active</span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger">Inactive</span>
                                                        @endif
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script>
        @if(Illuminate\Support\Facades\Session::has('errors'))
        $(document).ready(function () {
            $('#exampleModal').modal({show: true});
        });
        @endif
    </script>

@stop
