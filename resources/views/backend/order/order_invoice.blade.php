@extends('backend.master.app')
@section('main')
    <style>
        .container {
            margin: 100px 30px;
            overflow: hidden;
            font-size: 10px;
        }

        .header {
            border-bottom: 1px silver solid;
            overflow: hidden;
            padding-bottom: 20px;
        }

        .header-name {
            font-size: 18px;
            text-align: right;
        }
        .header-name span{ display: block; font-size: 14px; }
        .bold {
            font-weight: bold;
        }

        .logo {
            width: 100px;
            height: 100px;
        }

        .float-left {
            float: left;
        }

        .float-right {
            float: right;
        }

        .title {
            text-align: center;
            margin-top: 50px;
        }

        .invoice-id {
            background: #ccccff;
            height: 50px;
            width: auto;
            overflow: hidden;
            padding: 0 10px 0 10px;
            margin-bottom: 20px;
        }

        .bill {
            padding: 0 10px 0 10px;
        }

        .bill-name {
            padding-left: 10px;
            overflow: hidden;
        }
        .bill-right span,
        .bill-name span{ display: block; font-size: 14px; }
        .billRight{ float: right; }
        .inv-body {
            margin-bottom: 20px;
        }

        .inv-body table {
            width: 100%;
            border: 1px solid silver;
            border-collapse: collapse;
        }

        .inv-body table th, .inv-body table td {
            padding: 10px;
            border: 1px solid silver;
        }

        .inv-body table td h5, .inv-body table td p {
            margin: 0 5px 0 0;
        }

        .inv-footer {
            border-top: 1px silver solid;
            text-align: center;
        }

        .button {
            background: #1f6fb2;
        }

        .project-name {
            text-align: center;
        }
    </style>
    <div class="container" style="margin: 0;">
        <div id="divToPrint">
            <style>
                .container {
                    margin: 50px 30px;
                    overflow: hidden;
                    font-size: 10px;
                }

                .header {
                    border-bottom: 1px silver solid;
                    overflow: hidden;
                    padding-bottom: 20px;
                }

                .header-name {
                    font-size: 18px;
                    text-align: right;
                }

                .bold {
                    font-weight: bold;
                }

                .logo {
                    width: 100px;
                    height: 100px;
                }

                .float-left {
                    float: left;
                }

                .float-right {
                    float: right;
                }

                .title {
                    text-align: center;
                    margin-top: 10px;
                }

                .invoice-id {
                    background: #ccccff;
                    height: 50px;
                    width: auto;
                    overflow: hidden;
                    padding: 0 10px 0 10px;
                    margin-bottom: 20px;
                }

                .bill {
                    padding: 0 10px 0 10px;
                }

                .bill-name {
                    padding-left: 10px;
                    overflow: hidden;
                }

                .inv-body {
                    margin-bottom: 20px;
                }

                .inv-body table {
                    width: 100%;
                    border: 1px solid silver;
                    border-collapse: collapse;
                    font-size: 14px;
                }

                .inv-body table th, .inv-body table td {
                    padding: 10px;
                    border: 1px solid silver;
                }

                .inv-body table td h5, .inv-body table td p {
                    margin: 0 5px 0 0;
                }

                .inv-footer {
                    border-top: 1px silver solid;
                    text-align: center;
                }
                .inv-footer span{ display: block; font-size: 12px; }

                .button {
                    background: #1f6fb2;
                }

                .project-name {
                    text-align: center;
                }
            </style>
            <div class="container" style="margin: 0">
                <div class=" header">
                    <div class="float-left">
                        <img style="height: 100px;width: 100px;" class="logo"
                             src="{{asset('/uploads/setting/'.$business->logo)}}" alt="logo">
                    </div>
                    <div class="float-right header-name">
                        <div class="bold">{{$business->company_name}}</div>
                        <div>{{$business->address}}</div>
                        <div>Phone : {{$business->phone_number}}</div>
                        <div>Email : {{$business->email}}</div>
                    </div>
                </div>
                <h2 class="title">INVOICE</h2>

                <div class=" float-left" style="width: 45%;">
                    <div style="font-size: 10px">
                       <b> Billed to:</b>
                    </div>

                    <div class="bill-name">
                        <div>{{optional($order->user)->name}}</div>
                        <div>Phone: {{optional($order->user)->phone_number}}</div>
                        <div>Email: {{optional($order->user)->email}}</div>
                    </div>
                </div>
                <div style="width: 55%;" class="float-right">
                    <div style="padding-top: 20px">
                        <div style="font-size: 10px">
                            <b> Billed from:</b>
                        </div>
                        <div class="bill-right">
                            <div class="billRight">
                                <div class=""> {{$order->order_number}} <b>:INVOICE ID</b></div>
                                <div class="">{{$order->created_at->format(date('Y-m-d'))}} <b>:DATE</b></div>
                            </div>

                            <div>Delivered to :{{$order->receiver_name}}</div>
                            <div>Address :{{$order->receiver_address}}</div>
                            <div>Contact :{{$order->receiver_phone}}</div>
                            <div>Email :{{$order->email}}</div>
                        </div>
                    </div>
                </div>
                <div class="inv-body">
                    <table>
                        <tr>
                            <th>SL</th>
                            <th>Product</th>
                            <th>QT.</th>
                            <th>Discount.</th>
                            <th>Unit Price</th>
                            <th>Total</th>
                        </tr>
                        @php
                            $total=0;
                        @endphp
                        @foreach($order->orderDetails as $key=>$detail)
                            <tr>
                                <td>{{$key+1}}</td>
                                <td>{{optional($detail->product)->title}}</td>
                                <td>{{$detail->quantity}}</td>
                                <td>{{floor( optional($detail->product)->discount)}} %</td>
                                <td>৳ {{$detail->unit_price}}</td>
                                <td><strike>৳ {{$detail->unit_price*$detail->quantity}}</strike>
                                    ৳ {{$detail->sub_total}}</td>
                            </tr>
                            @php
                                $total=(float)$total+(float)$detail->sub_total;
                            @endphp
                        @endforeach
                        <tr>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td>Subtotal</td>
                            <td>{{$total}}.00 BDT</td>
                        </tr>
                        <tr>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td>Delivery Charge</td>
                            <td> ৳ {{$business->delivery_charge}}</td>
                        </tr>
                        <tr>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td style="border: none"></td>
                            <td>Total Payable</td>
                            <td>   ৳ {{$order->total}}</td>
                        </tr>
                    </table>
                </div>
                <div>
                    <h4 class="title">
                        Thank you for your cooperation!
                    </h4>
                    <div class="inv-footer">
                        <span class="bold">{{$business->company_name}} - A Software Development Company</span>
                        <span>{{$business->address}}</span>
                        <span>Email: {{$business->email}}</span>
                    </div>
                </div>

            </div>
        </div>
        <form class="print_order">
            <input class="btn btn-primary float-right" type="button" onClick="PrintDiv();" value="Print">
        </form>
        <a href="{{route('orders.index')}}" class="btn btn-danger">Back</a>
    </div>

    @stop
    <script language="javascript">
        function PrintDiv() {
            var divToPrint = document.getElementById('divToPrint');
            var popupWin = window.open('', '_blank', 'width=1100,height=700');
            popupWin.document.open();
            popupWin.document.write('<body onload="window.print()">' + divToPrint.innerHTML + '</html>');
            popupWin.document.close();
        }
    </script>


