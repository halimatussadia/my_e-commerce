@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">
                                        @if($status == 0 && $status != null)
                                            Pending Order list
                                        @elseif($status == 1)
                                            Process Order list
                                        @elseif($status == 2)
                                            Confirmed Order list
                                        @elseif($status == 3)
                                            Delivered Order list
                                        @elseif($status == 4)
                                            Received Order list
                                        @elseif($status == 5)
                                            Canceled Order list
                                        @else
                                            All Order list
                                        @endif
                                </div>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- Button trigger modal -->
                            <!-- Modal -->

                            <div class="card-body">
                                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1"
                                               class="table table-bordered table-striped dataTable dtr-inline"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Serial
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Browser: activate to sort column ascending">Customer
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Track Number
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Receiver Name
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Receiver Phone
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Date
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Status
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($orders as $key=>$order)
                                                <tr>
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{optional($order->user)->name}}</td>
                                                    <td><a href="{{route('orders.details',$order->id)}}">#{{$order->order_number}}</a></td>
                                                    <td>{{$order->receiver_name}}</td>
                                                    <td>{{$order->receiver_phone}}</td>
                                                    <td>{{$order->created_at->format(date('Y-m-d'))}}</td>
                                                    <td>
                                                        @if(array_key_exists($order->status,$getStatus))
                                                            <span class="badge p-2 font-size-h6 {{ $getStatus[$order->status] }}">
                                                        {{ ucfirst($order->status) }}</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <div class="dropdown">
                                                            <button class="btn btn-primary" id="dLabel" type="button" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false">
                                                                <i class="fa fa-ellipsis-h"></i>
                                                            </button>
                                                            <div class="dropdown-menu" aria-labelledby="dLabel">
                                                                <a class="dropdown-item btn btn-primary" target="_blank"
                                                                   href="{{route('orders.details',$order->id)}}"> <i
                                                                        class="fa fa-eye" ></i></a>
                                                                <a class="dropdown-item"
                                                                   href="{{route('orders.invoice',$order->id)}}">
                                                                    Invoice
                                                                </a>
                                                                @forEach($getStatus as $key=>$value)
                                                                    <a class="dropdown-item"
                                                                       href="{{ route('orders.status',['id'=>$order->id,'status'=>$key]) }}">
                                                                        <i class="fas fa-check-circle text-success">
                                                                            <span class="ml-2">{{ucfirst($key)}}</span>
                                                                        </i>
                                                                    </a>
                                                                @endforeach

                                                            </div>
                                                        </div>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script>

        @if(Illuminate\Support\Facades\Session::has('errors'))
        $(document).ready(function () {
            $('#exampleModal').modal({show: true});
        });
        @endif
    </script>

@stop
