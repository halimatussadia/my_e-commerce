@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card spur-card rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="spur-card-icon">
                                <i class="fas fa-chart-bar"></i>
                                <span class="spur-card-title">Slider information</span>
                            </div>
                        </div>
                        <div class="card-body ">
                            <form action="{{route('sliders.update',$edit->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <img src="{{asset('/uploads/slider/'.$edit->slider_img)}}"
                                             style="height: 200px;width: 200px; margin-left: 450px;border-radius: 25%;object-fit: cover">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name1">Slider Heading</label>
                                            <input type="text"
                                                   class="form-control @error('slider_heading') is-invalid @enderror"
                                                   id="name1" name="slider_heading"
                                                   placeholder=" Enter slider heading"
                                                   value="{{old('slider_heading').$edit->slider_heading}}">
                                            @error('slider_heading') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name2">Description</label>
                                            <textarea  type="text"  rows="6" cols="50"
                                                      class="form-control @error('slider_description') is-invalid @enderror"
                                                      id="name2" name="slider_description"
                                                      placeholder=" Enter slider description">{{old('slider_description').$edit->slider_description}}</textarea>
                                            @error('slider_description') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name3">Button Text</label>
                                            <input type="text"
                                                   class="form-control @error('button_text') is-invalid @enderror"
                                                   id="name3" name="button_text"
                                                   placeholder=" Enter button text"
                                                   value="{{old('button_text').$edit->button_text}}">
                                            @error('button_text') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Slider Url</label>
                                            <input type="url"
                                                   class="form-control @error('slider_url') is-invalid @enderror"
                                                   id="name" name="slider_url"
                                                   placeholder="https://www.facebook.com/"   value="{{old('slider_url').$edit->slider_url}}">
                                            @error('slider_url') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="name">Slider Image</label>
                                            <input type="file"
                                                   class="form-control rounded-lg @error('slider_img') is-invalid @enderror"
                                                   id="name"
                                                   name="slider_img" placeholder="Enter slider_img"
                                                   value="{{old('slider_img').$edit->slider_img}}">
                                            @error('slider_img') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">Status</label>
                                            <select name="status" id="phone"
                                                    class="form-control rounded-lg font-weight-bold">
                                                <option value="1" {{$edit->status==1 ? 'selected' : ''}}>Active</option>
                                                <option value="0"  {{$edit->status==0 ? 'selected' : ''}}>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-info rounded-lg">
                                        <i class="fa fa-save mr-1 "></i>Save
                                    </button>
                                    <a href="{{route('sliders.index')}}"
                                       class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                        <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                        Back
                                    </a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
