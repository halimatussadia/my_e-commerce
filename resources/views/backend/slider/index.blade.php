@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">Slider</div>
                            </div>
                            <div class="d-flex">
                                <button type="button" class="btn d-flex rounded-lg mx-3 py-2 btn-success "
                                        data-toggle="modal"
                                        data-target="#exampleModal" style="margin-left:  52rem!important">
                                    <i class="fa fa-plus mt-1 mr-2"></i>
                                    New Slider
                                </button>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- Button trigger modal -->
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add Slider</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('sliders.store')}}" method="post"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Slider
                                                        Image</label>
                                                    <div class="col-sm-8">
                                                        <input type="file"
                                                               class="form-control rounded-lg @error('slider_img') is-invalid @enderror"
                                                               id="name"
                                                               name="slider_img" placeholder="Enter slider_img"
                                                               value="{{old('slider_img')}}">
                                                        @error('slider_img') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name1" class="col-sm-4 col-form-label">Slider
                                                        Heading</label>
                                                    <div class="col-sm-8">
                                                        <input type="text"
                                                               class="form-control @error('slider_heading') is-invalid @enderror"
                                                               id="name1" name="slider_heading"
                                                               placeholder=" Enter slider heading"
                                                               value="{{old('slider_heading')}}">
                                                        @error('slider_heading') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="name2"
                                                           class="col-sm-4 col-form-label">Description</label>
                                                    <div class="col-sm-8">
                                                        <textarea  type="text"
                                                               class="form-control @error('slider_description') is-invalid @enderror"
                                                               id="name2" name="slider_description"
                                                               placeholder=" Enter slider description">{{old('slider_description')}}</textarea>
                                                        @error('slider_description') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="name3"
                                                           class="col-sm-4 col-form-label">Button Text</label>
                                                    <div class="col-sm-8">
                                                        <input type="text"
                                                               class="form-control @error('button_text') is-invalid @enderror"
                                                               id="name3" name="button_text"
                                                               placeholder=" Enter button text"
                                                               value="{{old('button_text')}}">
                                                        @error('button_text') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Slider Url</label>
                                                    <div class="col-sm-8">
                                                        <input type="url"
                                                               class="form-control @error('slider_url') is-invalid @enderror"
                                                               id="name" name="slider_url"
                                                               placeholder="https://www.facebook.com/">
                                                        @error('slider_url') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-danger rounded-lg"
                                                        data-dismiss="modal">Close
                                                </button>
                                                <button type="submit" class="btn btn-success rounded-lg">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1"
                                               class="table table-bordered table-striped dataTable dtr-inline"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Serial
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Slider Image
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Browser: activate to sort column ascending">Slider Heading
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Button Text
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Slider Url
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Status
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($sliders as $key=>$slider)
                                                <tr>
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td><img src="{{asset('/uploads/slider/'.$slider->slider_img)}}" style="height: 50px;width: 50px;object-fit: cover"></td>
                                                    <td>{{$slider->slider_heading}}</td>
                                                    <td>{{$slider->button_text}}</td>
                                                    <td>{{$slider->slider_url}}</td>
                                                    <td>
                                                        @if($slider->status==1)
                                                            <span class="badge badge-pill badge-success">Active</span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger">Inactive</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('sliders.edit',$slider->id)}}"
                                                           class="btn btn-primary btn-sm"> <i class="fas fa-edit"></i> </a>
                                                        <a href="{{route('sliders.delete',$slider->id)}}"
                                                           class="btn btn-danger btn-sm"> <i class="fas fa-trash"></i> </a>
                                                    </td>

                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script>
       @if(Illuminate\Support\Facades\Session::has('errors'))
        $(document).ready(function () {
            $('#exampleModal').modal({show: true});
        });
        @endif
    </script>

@stop
