@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card spur-card rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="spur-card-icon">
                                <i class="fas fa-chart-bar"></i>
                                <span class="spur-card-title">Brand information</span>
                            </div>
                        </div>
                        <div class="card-body ">
                            <form action="{{route('brands.update',$edit->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <img src="{{asset('/uploads/brand/'.$edit->image)}}"
                                             style="height: 200px;width: 200px; margin-left: 450px;border-radius: 25%;object-fit: cover">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1"> Name<span class="required"
                                                                                             style="color: red">*</span></label>
                                            <input type="text" class="form-control rounded-lg font-weight-bold" name="name" id="exampleFormControlInput1"
                                                   value="{{old('name').$edit->name}}">
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Description<span class="required"
                                                                                                   style="color: red">*</span></label>
                                            <textarea type="text" rows="6" cols="50" class="form-control rounded-lg font-weight-bold" name="description"
                                                      id="exampleFormControlInput1">{{old('description').$edit->description}}
                                            </textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="exampleFormControlInput1">Photo<span class="required"
                                                                                               style="color: red">*</span></label>
                                        <input type="file" class="form-control rounded-lg font-weight-bold" name="image"
                                               id="exampleFormControlInput1" value="{{old('image').$edit->image}}">
                                    </div>
                                </div>
                                <div class="col-md-6">
                                    <div class="form-group">
                                        <label for="phone">Status</label>
                                        <select name="status" id="phone" class="form-control rounded-lg font-weight-bold">
                                            <option value="1" @if($edit->status == 1) selected @endif>Active</option>
                                            <option value="0" @if($edit->status == 0) selected @endif>Inactive</option>
                                        </select>
                                    </div>
                                </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-info rounded-lg">
                                        <i class="fa fa-save mr-1 "></i>Save
                                    </button>
                                    <a href="{{route('brands.index')}}"
                                       class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                        <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                        Back
                                    </a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
