@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">Brand</div>
                            </div>
                            <div class="d-flex">
                                <button type="button" class="btn d-flex rounded-lg mx-3 py-2 btn-success "
                                        data-toggle="modal"
                                        data-target="#exampleModal" style="margin-left:  52rem!important">
                                    <i class="fa fa-plus mt-1 mr-2"></i>
                                    New Brand
                                </button>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- Button trigger modal -->
                            <!-- Modal -->
                            <div class="modal fade" id="exampleModal" tabindex="-1" role="dialog"
                                 aria-labelledby="exampleModalLabel" aria-hidden="true">
                                <div class="modal-dialog" role="document">
                                    <div class="modal-content">

                                        <div class="modal-header">
                                            <h5 class="modal-title" id="exampleModalLabel">Add Brand</h5>
                                            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
                                                <span aria-hidden="true">&times;</span>
                                            </button>
                                        </div>
                                        <div class="modal-body">
                                            <form action="{{route('brands.store')}}" method="post"
                                                  enctype="multipart/form-data">
                                                @csrf
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Name</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" required
                                                               class="form-control rounded-lg @error('name') is-invalid @enderror"
                                                               id="name"
                                                               name="name" placeholder="Enter name"
                                                               value="{{old('name')}}">
                                                        @error('name') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>

                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Description</label>
                                                    <div class="col-sm-8">
                                                        <input type="text" required
                                                               class="form-control @error('description') is-invalid @enderror"
                                                               id="name" name="description"
                                                               placeholder=" Enter description" value="{{old('description')}}">
                                                        @error('description') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <div class="form-group row">
                                                    <label for="name" class="col-sm-4 col-form-label">Photo</label>
                                                    <div class="col-sm-8">
                                                        <input type="file"
                                                               class="form-control @error('image') is-invalid @enderror"
                                                               id="name" name="image" value="{{old('image')}}">
                                                        @error('image') <span
                                                            class="text-danger">{{$message}}</span> @enderror
                                                    </div>
                                                </div>
                                                <button type="button" class="btn btn-danger rounded-lg"
                                                        data-dismiss="modal">Close
                                                </button>
                                                <button type="submit" class="btn btn-success rounded-lg">
                                                    <i class="fa fa-save"></i> Save
                                                </button>
                                            </form>
                                        </div>
                                        <div class="modal-footer">
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="card-body">
                                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1"
                                               class="table table-bordered table-striped dataTable dtr-inline"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Serial
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Name
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Browser: activate to sort column ascending">Image
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Status
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($brands as $key=>$brand)
                                                <tr>
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{$brand->name}}</td>
                                                    <td><img src="{{asset('/uploads/brand/'.$brand->image)}}" alt="photo" style="height: 50px;width: 50px;object-fit: cover"></td>
                                                    <td> @if($brand->status==1)
                                                            <span class="badge badge-pill badge-success">Active</span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger">Inactive</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('brands.edit',$brand->id)}}"
                                                           class="btn btn-primary btn-sm"> <i class="fas fa-edit"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script>
        @if(Illuminate\Support\Facades\Session::has('errors'))
        $(document).ready(function () {
            $('#exampleModal').modal({show: true});
        });
        @endif
    </script>

@stop
