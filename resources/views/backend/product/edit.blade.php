@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card spur-card rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="spur-card-icon">
                                <i class="fas fa-chart-bar"></i>
                                <span class="spur-card-title">Product information</span>
                            </div>
                        </div>
                        <div class="card-body ">
                            <form action="{{route('products.update',$edit->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <img src="{{asset('/uploads/product/'.json_decode($edit->image)[0] ?? null)}}"
                                             style="height: 200px;width: 200px; margin-left: 450px;border-radius: 25%;">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1"> Title<span class="required"
                                                                                              style="color: red">*</span></label>
                                            <input type="text"
                                                   class="form-control rounded-lg font-weight-bold @error('title') is-invalid @enderror"
                                                   name="title" id="exampleFormControlInput1"
                                                   value="{{old('title').$edit->title}}">
                                            @error('title') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Photo<span class="required"
                                                                                             style="color: red">*</span></label>
                                            <input type="file" class="form-control rounded-lg font-weight-bold"
                                                   name="image[]" multiple
                                                   id="exampleFormControlInput1" value="{{old('image')}}">
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Brand<span class="required"
                                                                                             style="color: red">*</span></label>
                                            <select
                                                class="form-control rounded-lg font-weight-bold @error('brand_id') is-invalid @enderror"
                                                name="brand_id" id="exampleFormControlInput1">
                                                @foreach($brands as $brand)
                                                    <option value="{{$brand->id}}" {{$brand->id==$edit->brand_id ?'selected':''}}>{{$brand->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('brand_id') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Category<span class="required"
                                                                                                style="color: red">*</span></label>
                                            <select
                                                class="form-control rounded-lg font-weight-bold @error('category_id') is-invalid @enderror"
                                                name="category_id" id="exampleFormControlInput1">
                                                @foreach($categories as $category)
                                                    <option value="{{$category->id}}" {{$category->id==$edit->category_id ?'selected':''}}>{{$category->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('category_id') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Discount<span class="required"
                                                                                                style="color: red">*</span></label>
                                            <input type="number"
                                                   class="form-control rounded-lg font-weight-bold @error('discount') is-invalid @enderror"
                                                   name="discount" id="exampleFormControlInput1"
                                                   value="{{old('discount').$edit->discount}}">
                                            @error('discount') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Unit Price<span class="required"
                                                                                                  style="color: red">*</span></label>
                                            <input type="number"
                                                   class="form-control rounded-lg font-weight-bold @error('unit_price') is-invalid @enderror"
                                                   name="unit_price" id="exampleFormControlInput1"
                                                   value="{{old('unit_price').$edit->unit_price}}">
                                            @error('unit_price') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Attribute<span class="required"
                                                                                                 style="color: red">*</span></label>
                                            <select class="js-example-basic-multiple form-control" name="attribute_id[]" multiple="multiple">
                                                @foreach($attributes as $attribute)

                                                    <option selected value="{{$attribute->id}}">{{$attribute->size}}</option>
                                                @endforeach
                                            </select>
                                            @error('attribute_id') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1"> Product Code<span class="required"
                                                                                              style="color: red">*</span></label>
                                            <input type="text"
                                                   class="form-control rounded-lg font-weight-bold @error('code') is-invalid @enderror"
                                                   name="code" id="exampleFormControlInput1"
                                                   value="{{old('code').$edit->code}}">
                                            @error('code') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-12">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Color<span class="required"
                                                                                             style="color: red">*</span></label>
                                            <select class="js-example-basic-multiple form-control @error('color_id') is-invalid @enderror"
                                                    name="color_id[]" multiple="multiple">
                                                @foreach($colors as $color)
                                                    <option selected value="{{$color->id}}">{{$color->name}}</option>
                                                @endforeach
                                            </select>
                                            @error('color_id') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Quantity<span class="required"
                                                                                                style="color: red">*</span></label>
                                            <input type="number"
                                                   class="form-control rounded-lg font-weight-bold @error('quantity') is-invalid @enderror"
                                                   name="quantity" id="exampleFormControlInput1"
                                                   value="{{old('quantity').optional($edit->stock)->quantity}}">
                                            @error('quantity') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Description<span class="required"
                                                                                                   style="color: red">*</span></label>
                                            <textarea type="text" rows="6" cols="50"
                                                      class="form-control rounded-lg font-weight-bold @error('description') is-invalid @enderror"
                                                      name="description" id="exampleFormControlInput1">
                                                    {{old('description').$edit->description}}</textarea>
                                            @error('description') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">Status</label>
                                            <select name="status" id="phone"
                                                    class="form-control rounded-lg font-weight-bold">
                                                <option value="1" @if($edit->status == 1) selected @endif>Active</option>
                                                <option value="0" @if($edit->status == 0) selected @endif>Inactive</option>
                                            </select>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone">Feature</label>
                                            <select name="is_featured" id="phone"
                                                    class="form-control rounded-lg font-weight-bold">
                                                <option value="1" @if($edit->is_featured == 1) selected @endif>Yes</option>
                                                <option value="0" @if($edit->is_featured == 0) selected @endif>No</option>
                                            </select>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-info rounded-lg">
                                        <i class="fa fa-save mr-1 "></i>Save
                                    </button>
                                    <a href="{{route('products.index')}}"
                                       class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                        <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                        Back
                                    </a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
