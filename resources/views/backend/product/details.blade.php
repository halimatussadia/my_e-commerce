@extends('backend.master.app')
@section('main')

@push('css')

@endpush
    <!-- END Hero -->
        <!-- Page Content -->
        <div class="content">
            <div class="block-rounded block-bordered">
                <div class="row">
                    <div class=" col-lg-5 col-xl-5">
                        <div class="card bg-light shadow shadow-sm">
                            <div class="card-body">

                                <img style="width: 350px"
                                     src="{{asset('/uploads/product/'.json_decode($product->image)[0] ?? null)}}" alt="product">
                            </div>
                        </div>
                    </div>

                    <div class=" col-lg-7 col-xl-7">
                        <div class="card bg-light shadow shadow-sm p-2">

                            <div class="card-header">
                                <div class="pb-2 pt-2 text-right ">
                                    <a class="btn btn-primary" href="{{route('products.edit',$product->id)}}">Edit</a>
                                    <a class="btn btn-danger" href="{{route('products.index')}}">Go
                                        back</a>
                                </div>
                                <h3>Title: {{$product->title}}</h3>

                            </div>
                            {{--                            @dd($product)--}}
                            <div class="card-body bg-light">
                                <p>Product Code: {{optional($product)->code}}</p>
                                <p>Description: {{$product->description}}</p>
                                <p>Unit Price : <span class="badge badge-primary">
                                       <span class="font-size-h4">৳</span>

                                        {{$product->unit_price}}</span>
                                </p>
                                <p>Size :
                                    @foreach($attributes as $attribute)
                                    <span class="badge badge-primary">
                                        {{optional($attribute)->size}}</span>
                                    @endforeach
                                </p>
                                <p>Color :
                                    @foreach($colors as $color)
                                        <span class="badge badge-primary">
                                        {{optional($color)->name}}</span>
                                    @endforeach
                                </p>
                                <p>Stock :
                                    @if(optional($product->stock)->quantity)
                                        <span class="badge badge-primary">Available</span>
                                    @else
                                        <span class="badge badge-danger">Out of stock</span>
                                    @endif
                                </p>
                                <p>Featured :
                                    @if($product->is_featured==1)
                                        <span class="badge badge-primary">Yes</span>
                                    @else
                                        <span class="badge badge-danger">No</span>
                                    @endif
                                </p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <!-- END Page Content -->

@endsection


@push('js')

@endpush
