@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">Product</div>
                            </div>
                            <div class="d-flex">
                                <a href="{{route('products.create')}}"
                                   class="btn d-flex rounded-lg mx-3 py-2 btn-success"
                                   style="margin-left:  40rem!important;">
                                    <i class="fa fa-plus mt-1 mr-2"></i>
                                    New Product
                                </a>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- Button trigger modal -->
                            <!-- Modal -->
                            <div class="card-body">
                                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1"
                                               class="table table-bordered table-striped dataTable dtr-inline"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Serial
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Title
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Browser: activate to sort column ascending">Image
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Browser: activate to sort column ascending">Code
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Price
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Status
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    is_features
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($products as $key=>$product)
                                                <tr>
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td>{{$product->title}}</td>
                                                    <td>
                                                        <img
                                                            src="{{asset('/uploads/product/'.collect((array)json_decode($product->image))->first() ?? null)}}"
                                                            alt="" style=" height: 50px; width: 60px;"></td>
                                                    <td>
                                                        <a href="{{route('products.details',$product->id)}}">{{$product->code}}</a>
                                                    </td>
                                                    <td>{{$product->unit_price}}</td>
                                                    <td> @if($product->status==1)
                                                            <span class="badge badge-pill badge-success">Active</span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger">Inactive</span>
                                                        @endif
                                                    </td>
                                                    <td> @if($product->is_featured==1)
                                                            <span class="badge badge-pill badge-success">Yes</span>
                                                        @else
                                                            <span class="badge badge-pill badge-danger">No</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('products.edit',$product->id)}}"
                                                           class="btn btn-primary btn-sm"> <i class="fas fa-edit"></i>
                                                        </a>
                                                        <a href="{{route('products.details',$product->id)}}"
                                                           class="btn btn-success btn-sm"> <i class="fas fa-eye"></i>
                                                        </a>

                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script>
        @if(Illuminate\Support\Facades\Session::has('errors'))
        $(document).ready(function () {
            $('#exampleModal').modal({show: true});
        });
        @endif
    </script>

@stop
