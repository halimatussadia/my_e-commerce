@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">Employee</div>
                            </div>
                            <div class="d-flex">
                                <a href="{{route('employees.create')}}"
                                   class="btn d-flex rounded-lg mx-5 py-2 btn-success "
                                   data-target="#exampleModal" style="margin-left:  52rem!important; font-size: 15px">
                                    <i class="fa fa-plus mt-1 mr-2"></i>
                                    New_Employee
                                </a>
                            </div>
                        </div>
                        <div class="card-body">

                            <!-- Button trigger modal -->
                            <!-- Modal -->
                            <div class="card-body">
                                <div id="example1_wrapper" class="dataTables_wrapper dt-bootstrap4">
                                </div>
                                <div class="row">
                                    <div class="col-sm-12">
                                        <table id="example1"
                                               class="table table-bordered table-striped dataTable dtr-inline"
                                               role="grid" aria-describedby="example1_info">
                                            <thead>
                                            <tr role="row">
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Serial
                                                </th>
                                                <th class="sorting_asc" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1" aria-sort="ascending"
                                                    aria-label="Rendering engine: activate to sort column descending">
                                                    Image
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Browser: activate to sort column ascending">Name
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Role
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Email
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Platform(s): activate to sort column ascending">
                                                    Contact
                                                </th>

                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Status
                                                </th>
                                                <th class="sorting" tabindex="0" aria-controls="example1"
                                                    rowspan="1" colspan="1"
                                                    aria-label="Engine version: activate to sort column ascending">
                                                    Action
                                                </th>
                                            </tr>
                                            </thead>
                                            <tbody>
                                            @foreach($employees as $key=>$employee)
                                                <tr>
                                                    <th scope="row">{{$key+1}}</th>
                                                    <td><img src="{{asset('/uploads/user/'.$employee->image)}}" alt=""
                                                             style="height: 50px;width:60px;object-fit: cover"></td>
                                                    <td>{{$employee->name}}</td>
                                                    <td>{{$employee->role}}</td>
                                                    <td>{{$employee->email}}</td>
                                                    <td>{{$employee->phone_number}}</td>
                                                    <td>
                                                        @if($employee->status===1)
                                                            <span class="badge badge-primary">Active</span>
                                                        @else
                                                            <span class="badge badge-danger">Inactive</span>
                                                        @endif
                                                    </td>
                                                    <td>
                                                        <a href="{{route('employees.edit',$employee->id)}}"
                                                           class="btn btn-primary btn-sm"> <i class="fas fa-edit"></i> </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
@section('javascript')
    <script>
        $(document).ready(function () {
            $('#user_dt').DataTable();
        });
        @if(Illuminate\Support\Facades\Session::has('errors'))
        $(document).ready(function () {
            $('#exampleModal').modal({show: true});
        });
        @endif
    </script>

@stop
