@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">Employee Create</div>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Button trigger modal -->
                            <!-- Modal -->
                            <div class="modal-body">
                                <form action="{{route('employees.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1"> Name<span class="required"
                                                                                                  style="color: red">*</span></label>
                                                <input type="text" class="form-control rounded-lg font-weight-bold @error('name') is-invalid @enderror"
                                                       name="name" id="exampleFormControlInput1" value="{{old('name')}}">
                                                @error('name') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Photo<span class="required"
                                                                                                 style="color: red">*</span></label>
                                                <input type="file" class="form-control rounded-lg font-weight-bold" name="image"
                                                       id="exampleFormControlInput1" value="{{old('image')}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Email<span class="required"
                                                                                                       style="color: red">*</span></label>
                                                <input type="email"  class="form-control rounded-lg font-weight-bold @error('email') is-invalid @enderror"
                                                          name="email" id="exampleFormControlInput1" value="{{old('email')}}">
                                                @error('name') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Address<span class="required"
                                                                                                 style="color: red">*</span></label>
                                                <input type="text"  class="form-control rounded-lg font-weight-bold @error('address') is-invalid @enderror"
                                                       name="address" id="exampleFormControlInput1" value="{{old('address')}}">
                                                @error('address') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Phone Number<span class="required"
                                                                                                 style="color: red">*</span></label>
                                                <input type="text"  class="form-control rounded-lg font-weight-bold @error('phone_number') is-invalid @enderror"
                                                       name="phone_number" id="exampleFormControlInput1" value="{{old('phone_number')}}">
                                                @error('phone_number') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Role<span class="required"
                                                                                                   style="color: red">*</span></label>
                                                <select class="form-control @error('role') is-invalid @enderror"
                                                        id="role" name="role">
                                                    <option value="admin">Admin</option>
                                                    <option value="contentManager">Content Manager</option>
                                                    <option value="salesManager">Sales Manager</option>
                                                </select>
                                                @error('address') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Password<span class="required"
                                                                                                 style="color: red">*</span></label>
                                                <input type="password"  class="form-control rounded-lg font-weight-bold @error('password') is-invalid @enderror"
                                                       name="password" id="exampleFormControlInput1" value="{{old('password')}}">
                                                @error('password') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-info rounded-lg">
                                            <i class="fa fa-save mr-1 "></i>Save
                                        </button>
                                        <a href="{{route('employees.index')}}"
                                           class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                            <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                            Back
                                        </a>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                {{--                {{ $users->links()}}--}}
            </div>
        </div>
    </section>
@stop
