@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card spur-card rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="spur-card-icon">
                                <i class="fas fa-chart-bar"></i>
                                <span class="spur-card-title">Employee information</span>
                            </div>
                        </div>
                        <div class="card-body ">
                            <form action="{{route('employees.update',$edit->id)}}" method="post"
                                  enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <img src="{{asset('/uploads/user/'.$edit->image)}}"
                                             style="height: 200px;width: 200px; margin-left: 450px;border-radius: 25%;object-fit: cover">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1"> Name<span class="required"
                                                                                             style="color: red">*</span></label>
                                            <input type="text"
                                                   class="form-control rounded-lg font-weight-bold @error('name') is-invalid @enderror"
                                                   name="name" id="exampleFormControlInput1"
                                                   value="{{$edit->name}}">
                                            @error('name') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Photo<span class="required"
                                                                                             style="color: red">*</span></label>
                                            <input type="file" class="form-control rounded-lg font-weight-bold"
                                                   name="image"
                                                   id="exampleFormControlInput1" value="{{old('image')}}">
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Email<span class="required"
                                                                                             style="color: red">*</span></label>
                                            <input type="email"
                                                   class="form-control rounded-lg font-weight-bold @error('email') is-invalid @enderror"
                                                   name="email" id="exampleFormControlInput1"
                                                   value="{{$edit->email}}">
                                            @error('name') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Address<span class="required"
                                                                                               style="color: red">*</span></label>
                                            <input type="text"
                                                   class="form-control rounded-lg font-weight-bold @error('address') is-invalid @enderror"
                                                   name="address" id="exampleFormControlInput1"
                                                   value="{{$edit->address}}">
                                            @error('address') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="exampleFormControlInput1">Phone Number<span class="required"
                                                                                                    style="color: red">*</span></label>
                                            <input type="text"
                                                   class="form-control rounded-lg font-weight-bold @error('phone_number') is-invalid @enderror"
                                                   name="phone_number" id="exampleFormControlInput1"
                                                   value="{{$edit->phone_number}}">
                                            @error('phone_number') <span
                                                class="text-danger">{{$message}}</span> @enderror
                                        </div>
                                    </div>
                                    @if(auth()->user()->role == 'admin' && auth()->user()->id == $edit->id)
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Password<span class="required"
                                                                                                    style="color: red">*</span></label>
                                                <input type="password"
                                                       class="form-control rounded-lg font-weight-bold @error('password') is-invalid @enderror"
                                                       name="password" id="exampleFormControlInput1"
                                                       value="{{old('password')}}">
                                                @error('password') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div class="row">
                                    <input type="hidden" name="role" value="{{$edit->role}}">
                                    <input type="hidden" name="status" value="{{$edit->status}}">
                                    @if(auth()->user()->role != 'admin')
                                        <input type="hidden" name="password" value="{{$edit->password}}">
                                    @endif
                                    @if(auth()->user()->id != $edit->id && auth()->user()->role == 'admin' )
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Role<span class="required"
                                                                                                style="color: red">*</span></label>
                                                <select class="form-control @error('role') is-invalid @enderror"
                                                        id="role" name="role" >
                                                    <option value="admin"
                                                            @if($edit->role == 'admin') selected @endif>Admin
                                                    </option>
                                                    <option value="contentManager"
                                                            @if($edit->role == 'contentManager') selected @endif>
                                                        Content Manager
                                                    </option>
                                                    <option value="salesManager"
                                                            @if($edit->role == 'salesManager') selected @endif>Sales
                                                        Manager
                                                    </option>
                                                </select>
                                                @error('address') <span
                                                    class="text-danger">{{$message}}</span> @enderror
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="phone_number">Status</label>
                                                <select class="form-control @error('status') is-invalid @enderror"
                                                        id="status" name="status">
                                                    <option value="1" @if($edit->status == 1) selected @endif>Active
                                                    </option>
                                                    <option value="0" @if($edit->status == 0) selected @endif>Inactive
                                                    </option>
                                                </select>
                                                <span
                                                    class="text-danger">@error('status'){{$message}} @enderror
                                            </span>
                                            </div>
                                        </div>
                                    @endif
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-info rounded-lg">
                                        <i class="fa fa-save mr-1 "></i>Save
                                    </button>
                                    <a href="{{route('employees.index')}}"
                                       class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                        <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                        Back
                                    </a>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
