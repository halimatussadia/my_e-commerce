<aside class="main-sidebar sidebar-dark-primary elevation-4">
    <!-- Brand Logo -->
    <a href="" class="brand-link">
        <img src="{{asset('/backend/img/AdminLTELogo.png')}}" alt="AdminLTE Logo"
             class="brand-image img-circle elevation-3"
             style="opacity: .8">
        <span class="brand-text font-weight-light"><img src="{{asset('/uploads/setting/'.$business->logo)}}" style="width: 140px; object-fit:
        cover;margin-top: 15px; height: 40px"/></span>
    </a>

    <!-- Sidebar -->
    <div class="sidebar">
        <!-- Sidebar user panel (optional) -->
        <div class="user-panel mt-3 pb-3 mb-3 d-flex">
            <div class="image">
                <img src="{{asset('/uploads/user/'.auth()->user()->image)}}" class="img-circle elevation-2"
                     alt="User Image">
            </div>
            <div class="info">
                <a href="#" class="d-block">{{auth()->user()->name}}</a>
            </div>
        </div>

        <!-- Sidebar Menu -->
        <nav class="mt-2">
            <ul class="nav nav-pills nav-sidebar flex-column" data-widget="treeview" role="menu" data-accordion="false">
                <!-- Add icons to the links using the .nav-icon class
                     with font-awesome or any other icon font library -->
                <li class="nav-item has-treeview">
                    <a href="{{route('home')}}" class="nav-link active">
                        <i class="nav-icon fas fa-tachometer-alt"></i>
                        <p>
                            Dashboard
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ request()->is('admin/orders/list') ? 'menu-open' : '' }}{{ request()->is('admin/orders/list/*') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link ">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            <i class="fas fa-angle-left right"></i>
                        </p>
                        <span class="nav-main-link-name">Orders
                            <span class="badge badge-warning ">{{$totals->pending}}</span>
                            <span class="badge badge-info ">{{$totals->confirmed}}</span>
                            <span class="badge badge-primary ">{{$totals->received}}</span>
                        </span>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a class="nav-link  {{ request()->is('admin/orders/list/pending') ? ' active' : '' }}"
                               href="{{route('orders.index','pending')}}">
                                <span class="nav-main-link-name">Pending Orders  <span class="badge badge-warning ">{{$totals->pending}}</span></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('admin/orders/list/processing') ? ' active' : '' }}"
                               href="{{route('orders.index','processing')}}">
                                <span class="nav-main-link-name">Process Orders <span class="badge badge-info ">{{$totals->processing}}</span></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('admin/orders/list/confirmed') ? ' active' : '' }}"
                               href="{{route('orders.index','confirmed')}}">
                                <span class="nav-main-link-name">Confirmed Orders <span class="badge badge-primary ">{{$totals->confirmed}}</span></span>
                            </a>

                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('admin/orders/list/received') ? ' active' : '' }}"
                               href="{{route('orders.index','received')}}">
                                <span class="nav-main-link-name">Received Orders <span class="badge badge-success ">{{$totals->received}}</span></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('admin/orders/list/delivered') ? ' active' : '' }}"
                               href="{{route('orders.index','delivered')}}">
                                <span class="nav-main-link-name">Delivered Orders <span class="badge badge-secondary ">{{$totals->delivered}}</span></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link {{ request()->is('admin/orders/list/canceled') ? ' active' : '' }}"
                               href="{{route('orders.index','canceled')}}">
                                <span class="nav-main-link-name">Canceled Orders <span class="badge badge-danger ">{{$totals->canceled}}</span></span>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a class="nav-link"
                               href="{{route('orders.index')}}">
                                <span class="nav-main-link-name">All Orders <span class="badge badge-dark">{{$totals->total}}</span></span>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{ request()->is('admin/products/list') ? 'menu-open' : '' }}{{ request()->is('admin/products/list/*') ? 'menu-open' : '' }}">
                    <a href="#" class="nav-link">
                        <i class="nav-icon fas fa-copy"></i>
                        <p>
                            Product Setup
                            <i class="fas fa-angle-left right"></i>
                        </p>
                    </a>
                    <ul class="nav nav-treeview">
                        <li class="nav-item">
                            <a href="{{route('products.index')}}" class="nav-link  {{ request()->is('admin/products/list/0') ? ' active' : '' }}">
                                <i class="fab fa-product-hunt nav-icon"></i>
                                <p>Product Lists</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('categories.index')}}" class="nav-link">
                                <i class="fa fa-list-alt nav-icon"></i>
                                <p>Category List</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('stocks.index')}}" class="nav-link">
                                <i class="fa fa-list-alt nav-icon"></i>
                                <p>Stock Setup</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('attribute.index')}}" class="nav-link">
                                <i class="fa fa-list-alt nav-icon"></i>
                                <p>Attribute Setup</p>
                            </a>
                        </li>
                        <li class="nav-item">
                            <a href="{{route('color.index')}}" class="nav-link">
                                <i class="fa fa-list-alt nav-icon"></i>
                                <p>Color Setup</p>
                            </a>
                        </li>
                    </ul>
                </li>
                <li class="nav-item has-treeview {{ request()->is('admin/brands/list') ? 'menu-open' : '' }}{{ request()->is('admin/brands/list/*') ? 'menu-open' : '' }}">

                <a href="{{route('brands.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Brand Lists
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ request()->is('admin/department/list') ? 'menu-open' : '' }}{{ request()->is('admin/department/list/*') ? 'menu-open' : '' }}">
                <a href="{{route('department.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-chart-pie"></i>
                        <p>
                            Department Lists
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ request()->is('admin/customers/list') ? 'menu-open' : '' }}{{ request()->is('admin/customers/list/*') ? 'menu-open' : '' }}">
                <a href="{{route('customers.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-users"></i>
                        <p>
                            Customer Lists
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ request()->is('admin/employees/list') ? 'menu-open' : '' }}{{ request()->is('admin/employees/list/*') ? 'menu-open' : '' }}">
                <a href="{{route('employees.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-user-tie"></i>
                        <p>
                            Employee List
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview {{ request()->is('admin/sliders/list') ? 'menu-open' : '' }}{{ request()->is('admin/sliders/list/*') ? 'menu-open' : '' }}">

                <a href="{{route('sliders.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-sliders-h"></i>
                        <p>
                            Slider List
                        </p>
                    </a>
                </li>

                <li class="nav-item">
                    <a href="{{route('blog.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-blog"></i>
                        <p>
                            Blog Lists
                        </p>
                    </a>
                </li>
                <li class="nav-item">
                    <a href="{{route('newsLetters.index')}}" class="nav-link">
                        <i class="nav-icon fas fa-calendar-alt"></i>
                        <p>
                            NewsLetter Lists
                        </p>
                    </a>
                </li>
                <li class="nav-item has-treeview{{ request()->is('admin/businessSettings/*') ? 'menu-open' : '' }}">
                <a href="{{route('businessSettings.edit',1)}}" class="nav-link">
                        <i class="nav-icon far fa-sun"></i>
                        <p>
                            Business Setting
                        </p>
                    </a>
                </li>
            </ul>
        </nav>
        <!-- /.sidebar-menu -->
    </div>
    <!-- /.sidebar -->
</aside>
