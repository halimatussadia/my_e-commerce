@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-xl-12">
                    <div class="card spur-card rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="spur-card-icon">
                                <i class="fas fa-chart-bar"></i>
                                <span class="spur-card-title">Business Setting information</span>
                            </div>
                        </div>
                        <div class="card-body ">
                            <form action="{{route('businessSettings.update',$edit->id)}}" method="post" enctype="multipart/form-data">
                                @csrf
                                <div class="form-row">
                                    <div class="form-group col-md-6">
                                        <img src="{{asset('/uploads/setting/'.$edit->logo)}}"
                                             style="width: 300px; margin-left: 450px;border-radius: 10%;
                                                 height: 100px;object-fit: cover ">
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="company_name">Company Name</label>
                                            <input type="text" class="form-control rounded-lg font-weight-bold @error('company_name') is-invalid @enderror" id="company_name"
                                                   name="company_name" value="{{$edit->company_name}}">
                                            <span
                                                class="text-danger">@error('company_name'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="address">Address</label>
                                            <input type="text" class="form-control rounded-lg font-weight-bold @error('address') is-invalid @enderror" id="address"
                                                   name="address" value="{{$edit->address}}">
                                            <span
                                                class="text-danger">@error('address'){{$message}} @enderror
                                             </span>
                                        </div>
                                    </div>
                                </div>

                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="email">Email</label>
                                            <input type="email" class="form-control @error('email') is-invalid @enderror" id="email"
                                                   name="email" value="{{$edit->email}}">
                                            <span
                                                class="text-danger">@error('email'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="phone_number">Phone Number</label>
                                            <input type="text" class="form-control @error('phone_number') is-invalid @enderror" id="phone_number"
                                                   name="phone_number" value="{{$edit->phone_number}}">
                                            <span
                                                class="text-danger">@error('phone_number'){{$message}} @enderror
                                             </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="web_address">Web Address</label>
                                            <input type="url" class="form-control @error('web_address') is-invalid @enderror" id="web_address"
                                                   name="web_address" value="{{$edit->web_address}}">
                                            <span
                                                class="text-danger">@error('web_address'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="hot_line">Hot Line</label>
                                            <input type="text" class="form-control @error('hot_line') is-invalid @enderror" id="hot_line"
                                                   name="hot_line" value="{{$edit->hot_line}}">
                                            <span
                                                class="text-danger">@error('hot_line'){{$message}} @enderror
                                             </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="facebook">Facebook</label>
                                            <input type="url" class="form-control @error('facebook') is-invalid @enderror" id="facebook"
                                                   name="facebook" value="{{$edit->facebook}}">
                                            <span
                                                class="text-danger">@error('facebook'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="twitter">Twitter</label>
                                            <input type="url" class="form-control @error('twitter') is-invalid @enderror" id="twitter"
                                                   name="twitter" value="{{$edit->twitter}}">
                                            <span
                                                class="text-danger">@error('twitter'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="instagram">Instagram</label>
                                            <input type="url" class="form-control @error('instagram') is-invalid @enderror" id="instagram"
                                                   name="instagram" value="{{$edit->instagram}}">
                                            <span
                                                class="text-danger">@error('instagram'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="pinterest">Pinterest</label>
                                            <input type="url" class="form-control @error('pinterest') is-invalid @enderror" id="pinterest"
                                                   name="pinterest" value="{{$edit->pinterest}}">
                                            <span
                                                class="text-danger">@error('pinterest'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="youtube">youtube</label>
                                            <input type="url" class="form-control @error('youtube') is-invalid @enderror" id="youtube"
                                                   name="youtube" value="{{$edit->youtube}}">
                                            <span
                                                class="text-danger">@error('youtube'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="delivery_charge">Delivery Charge</label>
                                            <input type="number" class="form-control @error('delivery_charge') is-invalid @enderror" id="delivery_charge"
                                                   name="delivery_charge" value="{{$edit->delivery_charge}}">
                                            <span
                                                class="text-danger">@error('delivery_charge'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="tag_line">Tag Line</label>
                                            <input type="text" class="form-control @error('tag_line') is-invalid @enderror" id="tag_line"
                                                   name="tag_line" value="{{$edit->tag_line}}">
                                            <span
                                                class="text-danger">@error('tag_line'){{$message}} @enderror
                                            </span>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="about">About</label>
                                            <textarea class="form-control" id="about" name="about"
                                                      rows="4" >{{$edit->about}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div class="row">
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <input type="file" class="custom-file-input js-custom-file-input-enabled"
                                                   data-toggle="custom-file-input" id="imag"
                                                   name="logo">
                                            <label class="custom-file-label" for="example-file-input-multiple-custom"
                                                   style="overflow-x: hidden;"></label>
                                        </div>
                                    </div>
                                    <div class="col-md-6">
                                        <div class="form-group">
                                            <label for="about">Terms & Conditions</label>
                                            <textarea class="form-control" id="about" name="terms_and_conditions"
                                                      rows="4" >{{$edit->terms_and_conditions}}</textarea>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <button type="submit" class="btn btn-info rounded-lg">
                                        <i class="fa fa-save mr-1 "></i>Save
                                    </button>
                                </div>

                            </form>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
@stop
