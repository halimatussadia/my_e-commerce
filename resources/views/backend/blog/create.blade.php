@extends('backend.master.app')
@section('main')
    <section class="content">
        <div class="container-fluid">
            <div class="row">
                <div class="col-lg-12">
                    <div class="card spur-card border-0 shadow rounded-lg">
                        <div class="card-header d-flex justify-content-between">
                            <div class="d-flex">
                                <div class="spur-card-icon">
                                    <i class="fas fa-table"></i>
                                </div>
                                <div class="spur-card-title">Blog Create</div>
                            </div>
                        </div>
                        <div class="card-body">
                            <!-- Button trigger modal -->
                            <!-- Modal -->
                            <div class="modal-body">
                                <form action="{{route('blog.store')}}" method="post" enctype="multipart/form-data">
                                    @csrf
                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1"> Title<span class="required"
                                                                                                  style="color: red">*</span></label>
                                                <input type="text" class="form-control rounded-lg font-weight-bold" name="title" id="exampleFormControlInput1"
                                                       value="{{old('title')}}">
                                            </div>
                                        </div>
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Photo<span class="required"
                                                                                                 style="color: red">*</span></label>
                                                <input type="file" class="form-control rounded-lg font-weight-bold" name="image"
                                                       id="exampleFormControlInput1" value="{{old('image')}}">
                                            </div>
                                        </div>
                                    </div>

                                    <div class="row">
                                        <div class="col-md-6">
                                            <div class="form-group">
                                                <label for="exampleFormControlInput1">Description<span class="required"
                                                                                                       style="color: red">*</span></label>
                                                <textarea type="text"  rows="10" cols="50" class="form-control rounded-lg font-weight-bold" name="description"
                                                          id="exampleFormControlInput1">{{old('description')}}
                                            </textarea>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <button type="submit" class="btn btn-info rounded-lg">
                                            <i class="fa fa-save mr-1 "></i>Save
                                        </button>
                                        <a href="{{route('blog.index')}}"
                                           class="bnt btn-warning text-decoration-none px-3 py-2 d-inline-block rounded-lg text-white font-weight-bold">
                                            <i class="fa fa-1x fa-arrow-circle-left mr-1"></i>
                                            Back
                                        </a>
                                    </div>

                                </form>

                            </div>
                        </div>
                    </div>
                </div>
                {{--                {{ $users->links()}}--}}
            </div>
        </div>
    </section>
@stop
