<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="utf-8">
    <title>{{config('app.name')}}</title>
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <!--[if ie]>
    <meta content='IE=8' http-equiv='X-UA-Compatible'/><![endif]-->
    <link href="https://fonts.googleapis.com/css?family=Josefin+Sans:300,300i,400,400i,700,700i" rel="stylesheet">

    <!-- bootstrap -->
    <link href="{{asset('/frontend/bootstrap/css/bootstrap.min.css')}}" rel="stylesheet">
    <link href="{{asset('/frontend/bootstrap/css/bootstrap-responsive.min.css')}}" rel="stylesheet">

    <link href="{{asset('/frontend/themes/css/bootstrappage.css')}}" rel="stylesheet"/>
    <link rel="stylesheet" href="{{asset('/frontend/css/flaticon.css')}}"/>
    <link rel="stylesheet" href="{{asset('/frontend/css/slicknav.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/frontend/css/jquery-ui.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/frontend/css/owl.carousel.min.css')}}"/>
    <link rel="stylesheet" href="{{asset('/frontend/css/animate.css')}}"/>
    <link rel="stylesheet" href="{{asset('/frontend/css/style.css')}}"/>
    <link rel="stylesheet" href="{{asset('/frontend/css/font-awesome.min.css')}}"/>

    <!-- global styles -->
    <link href="{{asset('/frontend/themes/css/flexslider.css')}}" rel="stylesheet"/>
    <link href="{{asset('/frontend/themes/css/main.css')}}" rel="stylesheet"/>
    @stack('css')
<!-- scripts -->
    <script src="{{asset('/frontend/themes/js/jquery-1.7.2.min.js')}}"></script>
    <script src="{{asset('/frontend/bootstrap/js/bootstrap.min.js')}}"></script>
    <script src="{{asset('/frontend/themes/js/superfish.js')}}"></script>
    <script src="{{asset('/frontend/themes/js/jquery.scrolltotop.js')}}"></script>
    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <script src="/frontend/js/respond.min.js"></script>
    <![endif]-->
    @notifyCss
</head>
<body>
@include('frontend.partials.navbar')
<div id="wrapper" class="container">
    @include('frontend.partials.main-menu')
    @include('notify::messages')
    @yield('main')
    @include('frontend.partials.footer')
</div>
</body>

<script src="{{asset('/frontend/themes/js/common.js')}}"></script>
<script src="{{asset('/frontend/themes/js/jquery.flexslider-min.js')}}"></script>
<script src="{{asset('/frontend/js/jquery.slicknav.min.js')}}"></script>
<script src="{{asset('/frontend/js/owl.carousel.min.js')}}"></script>
<script src="{{asset('/frontend/js/jquery.nicescroll.min.js')}}"></script>
<script src="{{asset('/frontend/js/jquery.zoom.min.js')}}"></script>
<script src="{{asset('/frontend/js/jquery-ui.min.js')}}"></script>
<script src="{{asset('/frontend/js/main.js')}}"></script>
<script type="text/javascript" src="{{asset('/frontend/js/zoomper.js')}}"></script>
@stack('js')
<script type="text/javascript">
    $(function () {
        $(document).ready(function () {
            $('.flexslider').flexslider({
                animation: "fade",
                slideshowSpeed: 4000,
                animationSpeed: 600,
                controlNav: false,
                directionNav: true,
                controlsContainer: ".flex-container" // the container that holds the flexslider
            });
        });
    });
</script>

@notifyJs
</html>

