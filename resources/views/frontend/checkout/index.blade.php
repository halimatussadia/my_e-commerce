@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Checkout
@stop
@section('main')

    <section class="header_text sub">
        <img class="pageBanner" src="{{asset('/uploads/slider/'.$sliders->slider_img)}}" style="height: 300px;object-fit: cover" alt="New products">
        <h4><span>Check Out</span></h4>
    </section>
    <section class="main-content">
        <div class="row">
            <div class="span12">
                <div class="accordion" id="accordion2">
                    <div class="accordion-group">
                        <div class="accordion-heading">
                            <a class="accordion-toggle" data-toggle="collapse" data-parent="#accordion2"
                               href="#collapseOne">Checkout Options</a>
                        </div>
                        <div id="collapseOne" class="accordion-body in collapse">
                            <div class="accordion-inner">
                                <div class="row-fluid">
                                    <div class="span6">
                                        <h4>Your Personal Details</h4>
                                        <form action="{{route('checkout.create')}}" method="post">
                                            @csrf()
                                            <fieldset>
                                                <div class="control-group">
                                                    <label class="control-label">First Name</label>
                                                    <div class="controls">
                                                        <input type="text" value="{{optional($user)->name}}"  name="receiver_name" placeholder="" class="input-xlarge" required>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Email</label>
                                                    <div class="controls">
                                                        <input type="email" name="email"  value="{{optional($user)->email}}" placeholder="you@example.com" class="input-xlarge" required>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Contact</label>
                                                    <div class="controls">
                                                        <input type="text" value="{{optional($user)->phone_number}}" placeholder="" name="receiver_phone" class="input-xlarge" required>
                                                    </div>
                                                </div>
                                                <div class="control-group">
                                                    <label class="control-label">Address</label>
                                                    <div class="controls">
                                                        <input type="text" value="{{optional($user)->address}}"  name="receiver_address"  placeholder="1234 Main St" class="input-xlarge" required>
                                                    </div>
                                                </div>
                                                <input type="hidden" name="delivery_charge" value="{{$business->delivery_charge}}">
                                                <input type="hidden" name="user_id">
                                                <input type="hidden" name="discount_price" value="{{$total_discount ?? ''}}">
                                                <input type="hidden" name="total"
                                                       value="{{$business->delivery_charge + $total}}">
                                                <br>
                                                <button class="btn btn-primary" type="submit">Place Order
                                                </button>
                                            </fieldset>
                                        </form>
                                    </div>
                                    <div class="span6">
                                        <h4>
                                            <a data-toggle="collapse" href="#order-cart-section" role="button"
                                               aria-expanded="true"
                                               aria-controls="order-cart-section"> products in Cart<span class="badge badge-primary"
                                                                                                         style="margin-left: 15px">
                                        {{session('cart') ? count(session('cart')) : '0'}}</span></a>
                                        </h4>

                                        <div class="span11">
                                            <table class="table table-striped">
                                                <thead>
                                                <tr>
                                                    <th>Product Name</th>
                                                    <th>Quantity</th>
                                                    <th>Size</th>
                                                    <th>Unit Price</th>
                                                    <th>Discount</th>
                                                    <th>Total</th>
                                                </tr>
                                                </thead>
                                                <tbody>
                                                @php

                                                    $i=1;
                                                    $carts = session('cart');
                                                @endphp
                                                @if(!empty($carts))
                                                    @foreach($carts as $key=>$cart)
                                                        <tr>
                                                            <td> <a href="{{route('product.details',$cart['product_id'])}}">{{$cart['title']}}</a>
                                                            </td>
                                                            <td>
                                                                <span class="badge badge-secondary">{{$cart['quantity']}}</span>
                                                            </td>
                                                            <td class="cart-product-sub-total">{{$cart['options']}}</td>
                                                            @php
                                                                $discount = $cart['unit_price']-($cart['unit_price']*$cart['discount']/100);
                                                                $actualDicount=$cart['unit_price']-$discount;
                                                            @endphp
                                                            <td class="cart-product-sub-total">{{$cart['unit_price']}} BDT</td>
                                                            <td class="cart-product-sub-total"> {{$actualDicount * $cart['quantity']}} BDT</td>
                                                            <td class="cart-product-grand-total">
                                                                <span class="cart-grand-total-price">{{ $cart['total_price']}} BDT</span>
                                                            </td>
                                                        </tr>
                                                    @endforeach
                                                @endif
                                                </tbody>
                                            </table>
                                            <hr>
                                            <p class="cart-total right">
                                                <strong>Sub-Total</strong>:	{{$total}}<br>
                                                <strong>Delivery charge</strong>: ৳ {{$business->delivery_charge}}<br>
                                                @php
                                                    $grandTotal = $total+$business->delivery_charge
                                                @endphp
                                                <strong>Total</strong>: {{$grandTotal}}<br>
                                            </p>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
  @endsection
