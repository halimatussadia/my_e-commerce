@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Product
@stop
@section('main')
    <section class="header_text sub">
        <img class="pageBanner" src="{{asset('/uploads/slider/'.$sliders->slider_img)}}" style="height: 300px;object-fit: cover" alt="New products">
    </section>
    <h4><span>New products</span></h4>
    <section class="main-content">

        <div class="container">
            <div class="my-wishlist-page">
                <div class="row">
                    <div class="span12 my-wishlist">
                        <div class="table-responsive">
                            <table class="table">
                                <thead>

                                </thead>
                                <tbody>
                                @foreach($wishlists as $wishlist)
                                    <tr>
                                        <td class="span4">
                                            <a href="{{route('product.details',$wishlist->products->id)}}">
                                                <img src="{{asset('/uploads/product/'.$wishlist->products->image)}}"
                                                     alt=""
                                                     style="height: 200px;width: 200px">
                                            </a>
                                        </td>
                                        <td class="span6">

                                            <h2
                                                class="product-name"><a
                                                    href="{{route('product.details',$wishlist->products->id)}}">{{$wishlist->products->title}}</a>
                                            </h2>

                                                <span class="price-before-discount">Price :<del> ৳ {{$wishlist->products->unit_price}}</del></span>
                                            <div class="product-price">Discount Price :<span class="price">৳ {{discount_cal($wishlist->products->unit_price,$wishlist->products->discount)}}  </span>
                                            </div>
                                        </td>
                                        <td class="span2 close-btn">
                                            <a href="{{route('wishlist.delete',$wishlist->id)}}"
                                               class="btn btn-danger"
                                               title="Add to Wish">
                                                <i class="fa fa-trash"></i>
                                            </a>
                                        </td>
                                    </tr>
                                @endforeach
                                </tbody>
                            </table>
                        </div>
                    </div>
                </div><!-- /.row -->
            </div><!-- /.sigin-in-->
        </div><!-- /.container -->
    </section>
@stop
