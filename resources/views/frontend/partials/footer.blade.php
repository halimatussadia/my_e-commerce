<section id="footer-bar">
    <div class="row">
        <div class="span3">
            <h4>Contact Us</h4>
            <ul class="nav" style="color: white">
                <li>Company Name: {{$business->company_name}}</li>
                <li>Address: {{$business->address}}</li>
                <li>Contact: {{$business->phone_number}}</li>
                <li>Email: {{$business->email}}</li>
                <li>Hot Line: {{$business->hot_line}}</li>
            </ul>
        </div>
        <div class="span4">
            <h4>My Account</h4>
            <ul class="nav">
                <li><a href="{{$business->facebook}}" target="_blank">Facebook : {{$business->facebook}}</a></li>
                <li><a href="{{$business->instagram}}" target="_blank">Instagram : {{$business->instagram}}</a></li>
                <li><a href="{{$business->twitter}}" target="_blank"> Twitter : {{$business->twitter}}</a></li>
                <li><a href="{{$business->pinterest}}" target="_blank">Pinterest : {{$business->pinterest}}</a></li>
                <li><a href="{{$business->youtube}}" target="_blank">Youtube : {{$business->youtube}}</a></li>
            </ul>
        </div>
        <div class="span5">
            <p class="logo"><img src="{{asset('/uploads/setting/'.$business->logo)}}"
              style="height: 60px;width: 60px;object-fit: cover" class="site_logo" alt=""></p>
            <p style="color: white">{{$business->about}}</p>
            <br/>
        </div>
        <div class="span6">
            <h4 class="widget-title">newsletter</h4>
            <form action="{{route('newsLetter.create')}}" method="post">
                @csrf
                <input type="email" class="form-control" name="email" placeholder="Email address" required>

                <button style="margin-bottom: 10px" type="submit" value="Subscribe">Subscribe</button>
            </form>
        </div>
    </div>
</section>
<section id="copyright">
    <span>ProfessionBd Soft &copy;  <?php echo date('Y')?>  <a href="https://www.facebook.com/ProfessionbdSoft/" target="_blank">E-commerce</a>.</span>
</section>
