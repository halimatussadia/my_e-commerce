<div id="top-bar" class="container">
    <div class="row">
        <div class="span4">
            <a href="{{route('home.index')}}" class="logo pull-left"><img src="{{asset('/uploads/setting/'.$business->logo)}}" class="site_logo" alt=""
                                                                          style="object-fit: cover;height:80px"></a>
        </div>
        <div class="span8">
            <div class="account pull-right">
                <ul class="user-menu">
                    @if(auth()->user())
                        <li><a href="{{route('customer.account')}}">
                                <i class="icon fa fa-user"></i>MY Account </a>
                        </li>
                        <li><a href="{{route('wishlist.index')}}">
                                <i class="icon fa fa-heart"></i>Wish list </a>
                        </li>
                    @endif
                    <li><a href="{{route('cart.index')}}">Your Cart<span class="badge "
                                                                         style="margin-left: 5px; background:red">
                                        {{session('cart') ? count(session('cart')) : '0'}}</span></a></li>
                    <li><a href="{{route('checkout.index')}}">Checkout</a></li>
                    @if(auth()->user())
                        <li><a href="{{route('logout.home',(auth()->user())->id)}}" class="">Logout</a></li>
                        <li>
                            <a href="{{route('customer.account')}}">Welcome {{(auth()->user())->name}}</a>
                        </li>
                    @else
                        <li><a href="{{route('registration')}}"><i class="icon fa fa-lock"></i>Login</a></li>
                    @endif
                </ul>
            </div>
        </div>
    </div>
</div>
