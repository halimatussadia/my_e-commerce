<style>
    /*.mega_wrap{ width: 100%; max-width: 470px;}*/
    .megaBox{ display: block; width: 260px; float: left;}
    .megaBox li{ width: 250px;}
    .megaPhoto{ float: left; width: 200px;}

#menu > ul > li:nth-child(6) > ul,
#menu > ul > li:nth-child(7) > ul,
#menu > ul > li:nth-child(8) > ul{ left: auto; right: 0; }


    /*.product-slider .owl-nav{ overflow: hidden}*/
</style>

<section class="navbar main-menu">
    <div class="navbar-inner main-menu">
        <nav id="menu" class="pull-right mt-5">
            <ul class="mega_wrap">
                @foreach($departments as $key=>$department)
                    <li><a href="">{{$department->name}}</a>
                        <ul style="background: lightcoral">
                            <div class="megaBox">
                            @foreach($department->category as $key=>$data)
                                <li><a href="{{route('categoryWiseProduct',$data->id)}}">{{$data->name}}</a></li>
                            @endforeach
                            </div>
                            <div class="megaPhoto">
                                <img src="{{asset('/uploads/department/'.$department->image)}}" alt=""/>
                            </div>
                        </ul>
                    </li>
                @endforeach
            </ul>
        </nav>
        <nav id="menu" class="pull-left">
            <ul>
                <li><a href="{{route('home.index')}}">Home</a></li>
            </ul>
        </nav>
    </div>
</section>
