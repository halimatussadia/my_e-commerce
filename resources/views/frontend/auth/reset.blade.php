@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Login Page
@stop
@section('main')
    <section class="header_text sub">
        <img class="pageBanner" src="{{asset('/uploads/slider/'.$sliders->slider_img)}}" style="height: 300px;object-fit: cover"alt="New products">
        <h4><span>Reset your Password</span></h4>
    </section>
    <section class="main-content">
        <div class="row">
            <div class="span12 text-center">
                <h4 class="title"><span class="text"><strong>Reset</strong> Password</span></h4>
                <form action="{{route('frontend.reset',$token)}}" method="post">
                    @csrf()
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label">Password</label>
                            <div class="controls">
                                <input type="password" name="password" placeholder="Enter your password" id="username" class="input-xlarge" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <input tabindex="3" class="btn btn-inverse large" type="submit"
                                   value="Reset password">
                            <hr>
                            <p class="reset"> <a tabindex="4" href="{{route('registration')}}" title="Recover your username or password">Go back to Login page</a></p>
                        </div>
                    </fieldset>
                </form>
            </div>
        </div>
    </section>
@endsection
