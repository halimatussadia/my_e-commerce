@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Login Page
@stop
@section('main')
    <section class="header_text sub">
        <h4><span>Login or Register</span></h4>
    </section>
    <section class="main-content">
        <div class="row">
            <div class="span6">
                <h4 class="title"><span class="text"><strong>Login</strong> Form</span></h4>
                <form action="{{route('login.add')}}" method="post">
                    @csrf()
                    <fieldset>
                        <div class="control-group">
                            <label class="control-label">Email</label>
                            <div class="controls">
                                <input type="text" name="email" placeholder="Enter your email" id="username" class="input-xlarge" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <label class="control-label">Password</label>
                            <div class="controls">
                                <input type="password" name="password" placeholder="Enter your password" id="password"
                                       class="input-xlarge" required>
                            </div>
                        </div>
                        <div class="control-group">
                            <input tabindex="3" class="btn btn-inverse large" type="submit"
                                   value="Sign into your account">
                            <hr>
                            <p class="reset">Recover your <a tabindex="4" href="{{route('forgot')}}" title="Recover your username or password">username or
                                    password</a></p>
                        </div>
                    </fieldset>
                </form>
            </div>
            <div class="span6">
                <h4 class="title"><span class="text"><strong>Register</strong> Form</span></h4>
                <form class="register-form outer-top-xs" role="form" action="{{route('registration.add')}}"
                      method="post"
                      enctype="multipart/form-data">
                    @csrf()
                    <div class="form-group">
                        <label class="info-title" for="exampleInputEmail2">Email Address <span>*</span></label>
                        <input type="email" name="email" class="input-xlarge unicase-form-control @error('email') is-invalid @enderror text-input"
                               id="exampleInputEmail2" value="{{old('email')}}"  placeholder="Enter your email" required>
                        @error('email') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="exampleInputEmail1">Name <span>*</span></label>
                        <input type="text" name="name" class="input-xlarge @error('name') is-invalid @enderror unicase-form-control text-input"
                               id="exampleInputEmail1" value="{{old('name')}}"  placeholder="Enter your name" required>
                        @error('name') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="exampleInputEmail7">Address <span>*</span></label>
                        <input type="text" name="address" class="input-xlarge @error('address') is-invalid @enderror unicase-form-control text-input"
                               id="exampleInputEmail7" value="{{old('address')}}"  placeholder="Enter your address" required>
                        @error('address') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="exampleInputEmail1">Phone Number <span>*</span></label>
                        <input type="number" name="phone_number"  placeholder="Enter your phone number" required
                               class="input-xlarge unicase-form-control @error('phone_number') is-invalid @enderror text-input" id="exampleInputEmail1" value="{{old('phone_number')}}">
                        @error('phone_number') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="exampleInputEmail1">Password <span>*</span></label>
                        <input type="password" name="password" placeholder="Enter your password" required
                               class="input-xlarge @error('password') is-invalid @enderror unicase-form-control text-input" id="exampleInputEmail1">
                        @error('password') <span class="text-danger">{{$message}}</span> @enderror
                    </div>
                    <div class="form-group">
                        <label class="info-title" for="exampleInputEmail1">Image</label>
                        <input type="file" name="image" class="input-xlarge unicase-form-control text-input"
                               id="exampleInputEmail1">
                    </div>

                    <input type="hidden" name="role" value="customer"/>
                         <br>
                    <button type="submit" class="btn btn-inverse large">Create your new account
                    </button>
                </form>
            </div>
        </div>
    </section>
   @endsection
