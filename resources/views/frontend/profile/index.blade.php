@extends('frontend.master')
@push('css')
    <link rel="stylesheet" type="text/css" href="{{asset('/frontend/css/customs.css')}}">
@endpush
@section('main')
    <section class="header_text sub">
        <img class="pageBanner" src="{{asset('/uploads/slider/'.$sliders->slider_img)}}"
             style="height: 300px;object-fit: cover" alt="New products">
        <h2><span>Home/Account</span></h2>
    </section>
    <section class="account-section">
        <div class="container">
            <div class="row">
                <div class="span3">
                    <ul id="nav-tabs-wrapper" class="nav nav-tabs nav-pills nav-stacked well">
                        <div class="usr-name">Hi {{ (auth()->user())->name }},</div>
                        <div class="nav-dash"><i class="fa fa-bell" aria-hidden="true"></i>Dashboard</div>
                        <li class="active"><a href="#orderHistory" data-toggle="tab">ORDER HISTORY</a></li>
                        <li><a href="#vtab2" data-toggle="tab">NEW ORDERS</a></li>
                        <div class="act-info">Account Information</div>
                        <li><a href="#profile" data-toggle="tab">PROFILE</a></li>
                    </ul>
                </div>
                <div class="span9">
                    <div class="tab-content">
                        <div role="tabpanel" class="tab-pane fade in active" id="orderHistory">
                            <div class="card-wrapper">
                                <div class="cart-top-sec">
                                    <h3 class="subTitle">History</h3>
                                </div>
                                <div class="card-body">
                                    <div class="row">
                                        <div class="span3 col4">
                                            <div class="account-wrapper">
                                                <div class="account-icon"><i class="fa fa-list" aria-hidden="true"></i>
                                                </div>
                                                <div class="account-body">
                                                    <p>Pending Orders</p>
                                                    <span class="price">{{ $totals->pending}}</span>
                                                </div>
                                            </div>
                                        </div>
                                        <div class="span3 col4">

                                            <div class="account-wrapper">
                                                <div class="account-icon ic-2"><i class="fa fa-list-ol"></i></div>

                                                <div class="account-body ac-2">
                                                    <p>Delivered Orders</p>
                                                    <span class="price">{{ $totals->delivered}}</span>
                                                </div>
                                            </div>

                                        </div>
                                        <div class="span3 col4">

                                            <div class="account-wrapper">
                                                <div class="account-icon ic-3"><i class="fa fa-list-alt"></i></div>

                                                <div class="account-body ac-3">
                                                    <p>Total Orders</p>
                                                    <span class="price">{{$totals->total}}</span>
                                                </div>
                                            </div>

                                        </div>
                                    </div>
                                </div>
                            </div>

                            <div id="DataTables_Table_0_wrapper" class="dataTables_wrapper dt-bootstrap4 no-footer">
                                <div class="" style="background: #f3f3f3; padding: 3px;">
                                    <div class="total-table pr-10">
                                        <div>
                                            <form class="register-form outer-top-xs" role="form" action="{{route('customer.account')}}" method="get">
                                            <h6 class="orderTxt">ORDER HISTORY</h6>
                                                <div class="formBox">
                                                    <input type="number" class="unicase-form-control text-input" id="exampleOrderId1" name="order_number">
                                                    <button type="submit" class="border-0"><i class="fa fa-search" aria-hidden="true"></i></button>
                                                </div>
                                            </form>
                                        </div>
                                        <div>
                                            <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                                <thead>
                                                <th>Order No</th>
                                                <th>Order Date</th>
                                                <th>Order Status</th>
                                                <th>Total Price</th>
                                                <th></th>
                                                </thead>
                                                <tbody>
                                                @foreach($orders as $order)
                                                    <tr>
                                                        <td>#{{ $order->order_number }}</td>
                                                        <td>{{ $order->created_at->format('Y-M-d') }}</td>
                                                        <td>

                                                            @if(array_key_exists($order->status,$status))
                                                                <h6 style="font-size: 12px">
                                                                    {{ $status[$order->status] }} </h6>
                                                            @endif
                                                        </td>
                                                        <td>{{ $order->total }}</td>
                                                        <td>
                                                            <a href="{{ route('checkout.details',$order->id) }}" class="d-inline-block ml-2">
                                                                <i class=" fa fa-eye"></i>
                                                            </a>
                                                        </td>
                                                    </tr>
                                                @endforeach
                                                </tbody>
                                            </table>
                                            {{ $orders->links() }}
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div role="tabpanel" class="tab-pane fade in" id="vtab2">
                            <div class="" style="background: #f3f3f3; padding: 3px;">
                                <div class="total-table">
                                    <div class="table-top">
                                        <h3 class="subTitle">New ORDER</h3>
                                    </div>
                                    <div>
                                        <table id="datatable" class="table table-striped table-bordered" style="width:100%">
                                            <thead>
                                            <th>Order No</th>

                                            <th>Order Date</th>
                                            <th>Order Status</th>
                                            <th>Total Price</th>
                                            <th>Action</th>
                                            </thead>
                                            <tbody>
                                            @foreach($new_orders as $new_order)
                                                <tr>
                                                    <td>#{{ $new_order->order_number }}</td>
                                                    <td>{{ $new_order->created_at->format('Y-M-d') }}
                                                    </td>
                                                    <td>
                                                        @if(array_key_exists($order->status,$status))
                                                            <h6 style="font-size: 12px">
                                                                {{ $status[$order->status] }} </h6>
                                                        @endif
                                                    </td>
                                                    <td>{{ $new_order->total }}</td>
                                                    <td>
                                                        <a href="{{ route('checkout.details',$new_order->id) }}" class="d-inline-block ml-2">
                                                            <i class=" fa fa-eye"></i>
                                                        </a>
                                                    </td>
                                                </tr>
                                            @endforeach
                                            </tbody>
                                        </table>
                                        {{ $new_orders->links() }}
                                    </div>
                                </div>
                            </div>
                        </div>


                        <div role="tabpanel" class="tab-pane fade in " id="profile">
                            <div class="body-content">
                                <div class="">
                                    <div class="sign-in-page ">
                                        <div class="clearfix" style="background: #f3f3f3; padding: 3px;">
                                            <!-- Sign-in -->
                                            <div class="span3 create-new-account">
                                                <h4 class="checkout-subtitle">Your Account</h4>
                                                <form class="register-form outer-top-xs" role="form" action="{{ route('customer.profile.update',$profiles->id) }}" method="post" enctype="multipart/form-data">
                                                    @csrf
                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputEmail1">Name
                                                            <span>*</span></label>
                                                        <input type="text" name="name" class="form-control unicase-form-control text-input" id="exampleInputEmail1" value="{{ $profiles->name }}">
                                                    </div>

                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputEmail2">Email Address
                                                            <span>*</span></label>
                                                        <input type="email" name="email" class="form-control unicase-form-control text-input" id="exampleInputEmail2" value="{{ $profiles->email }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputEmail1">Phone Number
                                                            <span>*</span></label>
                                                        <input type="number" name="phone_number" class="form-control unicase-form-control text-input" id="exampleInputEmail1" value="{{ $profiles->phone_number }}">
                                                    </div>
                                                    <div class="form-group">
                                                        <label class="info-title" for="exampleInputEmail1">Image
                                                            <span>*</span></label>
                                                        <input type="file" name="image" class="form-control unicase-form-control text-input" id="exampleInputEmail1">
                                                    </div>

                                                    <input type="hidden" name="role" value="customer" />
                                                    <button type="submit" class="btn-upper btn btn-primary checkout-page-button">
                                                        Update
                                                    </button>
                                                </form>
                                            </div>
                                            <div class="span4 create-new-account">
                                                <h4 class="checkout-subtitle left">Profile Photo</h4>
                                                <div class="pic">
                                                    <img src="{{ asset('/uploads/users/'.$profiles->image) }}" style="width: 350px; height: 400px">
                                                </div>
                                            </div>
                                            <!-- create a new account -->
                                        </div><!-- /.row -->
                                    </div><!-- /.sigin-in-->
                                    <!-- ============================================== BRANDS CAROUSEL ============================================== -->
                                </div><!-- /.body-content -->
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <!-- Home Account section start -->

    <!---shipping adrress modal --->
    @push('js')
        <script>

            document.getElementById('is-default').addEventListener('change', function () {
                fetch('http://example.com/movies.json')
                    .then((response) => {
                        return response.json();
                    })
                    .then((myJson) => {
                        console.log(myJson);
                    });
            })
        </script>
    @endpush
@stop
