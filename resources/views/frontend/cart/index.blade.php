@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Cart
@stop
@section('main')
			<section class="header_text sub">
			<img class="pageBanner" src="{{asset('/uploads/slider/'.$sliders->slider_img)}}" style="height: 300px;object-fit: cover" alt="New products" >
				<h4><span>Shopping Cart</span></h4>
			</section>
			<section class="main-content">
				<div class="row">
					<div class="span11">
						<h4 class="title"><span class="text"><strong>Your</strong> Cart</span></h4>
						<table class="table table-striped">
							<thead>
								<tr>
									<th>Remove</th>
									<th>Product Name</th>
									<th>Quantity</th>
									<th>Size</th>
									<th>colo</th>
									<th>Unit Price</th>
									<th>Discount</th>
									<th>Total</th>
								</tr>
							</thead>
							<tbody>
                            @php
                                $total =0;
                                $i=1;
                                $carts = session('cart');
                            @endphp
                            @php
                                @endphp
                            @if(!empty($carts))
                                @foreach($carts as $key=>$cart)
								    <tr>
                                        <td class="romove-item">
                                            <a href="{{route('delete.cart',$key)}}"
                                               style="color: red;font-weight: bold">x
                                            </a>
                                        </td>
									<td> <a href="{{route('product.details',$cart['product_id'])}}">{{$cart['title']}}</a>
                                    </td>
									<td>
                                        <form action="{{route('update.cart')}}" method="post">
                                            @csrf
                                            <input type="hidden" value="{{$key}}" name="cart_id">
                                           <div class="d-flex">
                                               <input style="width: 50px;" type="number" value="{{$cart['quantity']}}"
                                                      name="quantity">
                                               <button
                                                   type="submit" class="btn btn-primary">Update
                                               </button>
                                           </div>
                                        </form>
                                    </td>
                                        <td class="cart-product-sub-total">{{$cart['options']}}</td>
                                        <td class="cart-product-sub-total">{{$cart['color']}}</td>
                                        @php
                                            $discount = $cart['unit_price']-($cart['unit_price']*$cart['discount']/100);
                                            $actualDicount=$cart['unit_price']-$discount;
                                        @endphp
                                        <td class="cart-product-sub-total">{{$cart['unit_price']}} BDT</td>
                                        <td class="cart-product-sub-total"> {{$actualDicount * $cart['quantity']}} BDT</td>
                                        @php
                                            $price = $cart['total_price'];
                                        @endphp
                                        <td class="cart-product-grand-total">
                                            <span class="cart-grand-total-price">{{$price}} BDT</span>
                                        </td>
                                    </tr>
                                    @php
                                        $total = $total+ $price;
                                    @endphp
                                @endforeach
                            @endif
							</tbody>
						</table>
						<hr>
						<p class="cart-total right">
							<strong>Sub-Total</strong>:	{{$total}}<br>
							<strong>Delivery charge</strong>: ৳ {{$business->delivery_charge}}<br>
                            @php
                                $grandTotal = $total+$business->delivery_charge
                            @endphp
							<strong>Total</strong>: {{$grandTotal}}<br>
						</p>
						<hr/>
						<p class="buttons center">
                            @if(!empty('cart'))
                                <a   class="btn btn-upper btn-info"  href="{{route('destroy.cart')}}">Clear Cart</a>
                            @endif
                            <a href="{{route('home.index')}}"
                               class="btn btn-upper btn-primary outer-left-xs">Continue Shopping
                            </a>
							<a href="{{route('checkout.index')}}" class="btn btn-inverse" type="submit" id="checkout">Checkout</a>
						</p>
					</div>
				</div>
			</section>
            @endsection
