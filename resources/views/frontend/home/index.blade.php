@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Home
@stop
@section('main')
    <audio controls autoplay style="display: none;">
        <source src="frontend/themes/images/bgMusic.ogg" type="audio/ogg">
        <source src="frontend/themes/images/bgMusic.mp3" type="audio/mpeg">
    </audio>
    <section class="homepage-slider" id="home-slider">
        <div class="flexslider">
            <ul class="slides">
                @foreach($sliders as $slider)
                    <li>
                        <img src="{{asset('/uploads/slider/'.optional($slider)->slider_img)}}" alt=""/>
                        <div class="intro">
                            @if($slider->slider_heading)
                            <h1>{{$slider->slider_heading}}</h1>
                            @endif
                            @if($slider->button_text)
                                <p><a style="text-decoration: none"
                                      href="{{$slider->slider_url}}"><span>{{$slider->button_text}}</span></a></p>
                            @endif
                        </div>
                    </li>
                @endforeach
            </ul>
        </div>
    </section>
    <!-- letest product section -->
    <section class="top-letest-product-section">
        <div class="container">
            <div class="section-title">
                <h2>LATEST PRODUCTS </h2>
            </div>
            <div class="product-slider owl-carousel">
                @foreach($products as $product)
                <div class="product-item">
                    <div class="pi-pic">
                        <a href="{{route('product.details',$product->id)}}"><img
                                src="{{asset('/uploads/product/'.json_decode($product->image)[0] ?? null)}}" alt=""/></a>
                    </div>
                    <div class="pi-text">
                        <h5 style="color: black"><a href="{{route('product.details',$product->id)}}"
                               class="title">{{$product->title}}</a></h5>
                    </div>
                </div>
                @endforeach

            </div>
        </div>
    </section>
    <!-- letest product section end -->
    <section class="main-content">
        <div class="row">
            <div class="span12">
                <div class="row">
                    <div class="span12">

                        <div id="myCarousel" class="myCarousel carousel slide">
                            <div class="carousel-inner">
                                <div class="item active">
                                    <ul class="thumbnails">
                                        @foreach($feature_products as $feature_product)
                                            <li class="span6">
                                                <div class="product-box">
                                                    <span class="sale_tag"></span>
                                                    <p>
                                                        <a href="{{route('product.details',$feature_product->id)}}"><img
                                                                src="{{asset('/uploads/product/'.json_decode($feature_product->image)[0] ?? null)}}"
                                                                style="object-fit: cover;height: 100%;width: 100%" alt=""/></a>
                                                    </p>
                                                </div>
                                            </li>
                                        @endforeach
                                    </ul>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
                <br/>
            </div>
        </div>
    </section>
    <section class="banner-section">
        <div class="container">
            <div class="banner set-bg" data-setbg="{{asset('/uploads/product/'.json_decode($new_products->image)[0] ?? null)}}" style="height: 400px;object-fit: cover">
                <div class="tag-new">NEW</div>
                <span>New Arrivals</span>
                <a href="{{route('product.index')}}" class="site-btn">SHOP NOW</a>
            </div>
        </div>
        <div class="row feature_box">
            <div class="span4">
                <div class="service">
                    <div class="responsive">
                        <img src="{{asset('/frontend/themes/images/feature_img_2.png')}}" alt=""/>
                        <h4>MODERN <strong>DESIGN</strong></h4>
                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="service">
                    <div class="customize">
                        <img src="{{asset('/frontend/themes/images/feature_img_1.png')}}" alt=""/>
                        <h4>SHIPPING <strong>Charge</strong></h4>

                    </div>
                </div>
            </div>
            <div class="span4">
                <div class="service">
                    <div class="support">
                        <img src="{{asset('/frontend/themes/images/feature_img_3.png')}}" alt=""/>
                        <h4>24/7 LIVE <strong>SUPPORT</strong></h4>
                    </div>
                </div>
            </div>
        </div>
    </section>

@endsection
