@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Product Details
@stop
@section('main')
    <style>
        {
            -moz-box-sizing: border-box;
            -webkit-box-sizing: border-box;
            box-sizing: border-box;
            margin: 0;
            padding: 0;
        }

        .image-box {
            position: relative;
            margin: auto;
            overflow: hidden;
            width: 540px;
        }
        .image-box img {
            max-width: 100%;
            transition: all 0.3s;
            display: block;
            width: 100%;
            height: auto;
            transform: scale(1);
        }

        .image-box:hover img {
            transform: scale(1.1);
        }

    </style>
    <section class="header_text sub">
        <img class="pageBanner" src="{{asset('/uploads/slider/'.$sliders->slider_img)}}" style="height: 300px;" alt="New products">
        <h4><span>Product Details</span></h4>
    </section>
    <section class="main-content">
        <div class="row">
            <div class="span8">
                <div class="row">
                    <div class="span4">
                        <a href="{{asset('/uploads/product/'.json_decode($products->image)[0] ?? null)}}" target="_blank" class="thumbnail" data-fancybox-group="group1"><img class="image-box" alt="" src="{{asset('/uploads/product/'.json_decode($products->image)[0] ?? null)}}"></a>
                        <ul class="thumbnails small">
                            @foreach(json_decode($products->image) ?? [] as $image)

                            <li class="span1">
                                <a href="{{asset('/uploads/product/'.$image)}}" class="thumbnail" data-fancybox-group="group1" title="Description 2"><img src="{{asset('/uploads/product/'.$image)}}" alt=""></a>
                            </li>
                          @endforeach
                        </ul>
                    </div>
                    <div class="span4">
                        <form action="{{route('add.cart',$products->id)}}" method="get">
                            <address>
                                <p>Brand: {{optional($products->brand)->name}}</p>
                                <p>Product Title: {{$products->title}}</p>
                                <p>Product Code: {{optional($products)->code}}</p>
                                <p><strong>Size:</strong> <span>
                                         @foreach($attributes as $attribute)
                                        <input type="radio" value="{{$attribute->id}}" name="size">
                                        {{optional($attribute)->size}}
                                    @endforeach
                                    </span></p>
                                <p><strong>Color:</strong> <span>
                                        @foreach($colors as $color)
                                            <input type="radio" value="{{$color->id}}" name="name">
                                            {{optional($color)->name}}
                                            @endforeach
                                    </span></p>

                                <p><strong>Stock:</strong> <span>
                                        @if(optional($products->stock)->quantity)
                                        <span class="badge badge-primary">Available</span>
                                    @else
                                        <span class="badge badge-danger">Out of stock</span>
                                    @endif
                                    </span></p>
                            </address>
                            <p><strong><del>Price: {{$products->unit_price}}</del></strong></p>
                            <p><strong>Price: ৳ {{discount_cal($products->unit_price,$products->discount)}}</strong></p>
                            @if(optional($products->stock)->quantity !== 0)
                                <button type="submit"
                                        class="btn btn-success"
                                        title="Add to Cart">
                                    <i class="fa fa-shopping-cart"></i>Add to cart
                                </button>
                            @endif
                            <a href="{{route('add.wishlist',$products->id)}}"
                               class="btn btn-danger"
                               title="Add to Wish">
                                <i class="fa fa-heart"></i>WishList
                            </a>
                        </form>
                    </div>

                    <div class="span4">

                    </div>
                </div>
                <div class="row">
                    <div class="span12">
                        <ul class="nav nav-tabs" id="myTab">
                            <li class="active"><a href="#home">Description</a></li>
                        </ul>
                        <div class="tab-content">
                            <div class="tab-pane active" id="home">{{$products->description}}</div>
                        </div>
                    </div>
                    <section class="top-letest-product-section">
                        <div class="span12">
                            <div class="section-title">
                                <h2>Related PRODUCTS</h2>
                            </div>
                            <div class="product-slider owl-carousel">
                                @foreach($relatedProducts as $product)
                                    <div class="product-item">
                                        <div class="pi-pic">
                                            <a href="{{route('product.details',$product->id)}}"><img
                                                    src="{{asset('/uploads/product/'.json_decode($product->image)[0] ?? null)}}"
                                                    style="height: 250px;" alt=""/></a>
                                        </div>
                                        <div class="pi-text">
                                            <h4><a href="{{route('product.details',$product->id)}}"
                                                   class="title">{{$product->title}}</a></h4>
                                        </div>
                                        <p>
                                            <a href="{{route('add.wishlist',$product->id)}}"
                                               class="btn btn-danger"
                                               title="Add to Wish">
                                                <i class="fa fa-heart"></i>Wishlist
                                            </a>
                                        </p>
                                    </div>
                                @endforeach

                            </div>
                        </div>
                    </section>
                </div>
            </div>
            <div class="span3 col">
                <div class="block clearfix" style="width: 350px; overflow-y: scroll; max-height: 300px;">
                    <ul class="nav nav-list clearfix" style="padding-bottom: 15px;">
                        <li class="nav-header"><h5>Contact Us</h5></li>
                        <li><a href=""><strong>Company:</strong> {{$business->company_name}}</a></li>
                        <li><a href=""><strong>Address :</strong>{{$business->address}}</a></li>
                        <li><a href=""><strong>Contact :</strong>{{$business->phone_number}}</a></li>
                        <li><a href=""><strong>Email :</strong> {{$business->email}}</a></li>
                        <li><a href=""><strong>Hot_line : </strong>{{$business->hot_line}}</a></li>
                        <li><b><a href=""><strong>{{$business->tag_line}}</strong></a></b></li>
                    </ul>
                    
                    <ul class="nav nav-list below clearfix" style="padding-bottom: 15px;">
                        <li class="nav-header"><h5>Social</h5></li>
                        <li><a href="{{$business->facebook}}" target="_blank"><strong>Facebook :</strong> {{$business->facebook}}</a></li>
                        <li><a href="{{$business->instagram}}" target="_blank"><strong>Instagram :</strong> {{$business->instagram}}</a></li>
                        <li><a href="{{$business->twitter}}" target="_blank"> <strong>Twitter : </strong>{{$business->twitter}}</a></li>
                        <li><a href="{{$business->pinterest}}" target="_blank"><strong>Pinterest :</strong> {{$business->pinterest}}</a></li>
                        <li><a href="{{$business->youtube}}" target="_blank"><strong>Youtube :</strong> {{$business->youtube}}</a></li>
                    </ul>
                    <div class="nav nav-list below text-left" style="padding-bottom: 15px;">
                        <h5>Terms & Conditions</h5>
                        <p>{{$business->terms_and_conditions}}</p>
                    </div>
                </div>
            </div>
        </div>
    </section>
@endsection
<script>
    $(function () {
        $('#myTab a:first').tab('show');
        $('#myTab a').click(function (e) {
            e.preventDefault();
            $(this).tab('show');
        })
    })
    $(document).ready(function () {
        $('.thumbnail').fancybox({
            openEffect: 'none',
            closeEffect: 'none'
        });

        $('#myCarousel-2').carousel({
            interval: 2500
        });
    });

</script>
