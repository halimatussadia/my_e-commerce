@extends('frontend.master')
@section('title')
    {{config('app.name')}}-Product
@stop
@section('main')


    <section class="header_text sub">
        <img class="pageBanner" src="{{asset('/uploads/slider/'.$sliders->slider_img)}}"
             style="height: 300px;object-fit: cover" alt="New products">
        <h4><span>New products</span></h4>
    </section>

    <section class="main-content">
        <style type="text/css">
            .parant {
            }

            .parant.active {
            }

            .dropMenu {
                display: none;
            }

            .parant.active .dropMenu {
                display: block;
                padding-left: 20px;
                text-transform: capitalize;
            }
        </style>
        <div class="row">
            <div class="span3 col">
                <div class="block">
                    <ul class="nav nav-list">
                        <li class="nav-header"> CATEGORIES</li>
                        @foreach($departments as $key=>$department)
                            <li class="parant" style="cursor: pointer"><span
                                    style="font-weight: bold;font-size: 15px">{{$department->name}}</span>
                                <ul class="dropMenu">
                                    @foreach($department->category as $key=>$data)
                                        <li><a href="{{route('categoryWiseProduct',$data->id)}}">{{$data->name}}</a>
                                        </li>
                                    @endforeach
                                </ul>
                            </li>
                        @endforeach
                    </ul>
                </div>
            </div>
            <div class="span9">
                <ul class="thumbnails listing-products">
                    @foreach($products as $product)
                        <li class="span3">
                            <div class="product-box">
                                <span class="sale_tag"></span>
                                <p>
                                    <a href="{{route('product.details',$product->id)}}"><img
                                            src="{{asset('/uploads/product/'.json_decode($product->image)[0] ?? null)}}"
                                            style="height: 200px;" alt=""/></a>
                                </p>
                                <a href="{{route('product.details',$product->id)}}"
                                   class="title">{{$product->title}}</a><br/>

                                <div class="price-before-discount">
                                    <del> ৳ {{$product->unit_price}}</del>
                                    <p
                                        class="price"><span
                                            class="price">৳ {{discount_cal($product->unit_price,$product->discount)}}</span>
                                    </p>
                                    <p>
                                        <a href="{{route('add.wishlist',$product->id)}}"
                                           class="btn btn-danger"
                                           title="Add to Wish">
                                            <i class="fa fa-heart"></i>WishList
                                        </a>
                                    </p>
                                </div>
                            </div>
                        </li>
                    @endforeach
                </ul>
                <hr>
                <div class="pagination pagination-small pagination-centered">
                    <ul>
                        {{$products->links()}}
                    </ul>
                </div>
            </div>
        </div>
    </section>

@endsection
@push('js')

    <script type="text/javascript">
        $(document).ready(function () {
            $(".parant").click(function () {
                $(this).toggleClass("active");
            });
        });
    </script>
@endpush
