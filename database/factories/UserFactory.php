<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Models\User;
use Faker\Generator as Faker;
use Illuminate\Support\Str;

/*
|--------------------------------------------------------------------------
| Model Factories
|--------------------------------------------------------------------------
|
| This directory should contain each of the model factory definitions for
| your application. Factories provide a convenient way to generate new
| model instances for testing / seeding your application's database.
|
*/

$factory->define(User::class, function (Faker $faker) {
    return [
        'name' => $faker->userName,
        'phone_number' => $faker->phoneNumber,
        'role' => 'customer',
        'email' => $faker->unique()->safeEmail,
        'address' =>$faker->address,
        'email_verified'      =>0,
        'email_verified_at'   =>\Carbon\Carbon::now(),
        'email_verification_token'=>Str::random(30),
        'password' => bcrypt('123456'), // password        'remember_token' => Str::random(10),
    ];
});
