<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Slider::class, function (Faker $faker) {
    return [
        'slider_heading' => $faker->name,
        'slider_description' => $faker->realText(300),
        'button_text' => $faker->name,
        'slider_url' => $faker->url,
    ];
});
