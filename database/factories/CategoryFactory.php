<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Category::class, function (Faker $faker) {
    return [
        'name' => $faker->name('male'),
        'department_id' => random_int(1, 10),
        'description' => $faker->realText(),
        'status' => 1,
        'feature' => 0,
    ];
});
