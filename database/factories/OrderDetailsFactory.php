<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\OrderDetails::class, function (Faker $faker) {
    return [
        'order_id' => random_int(1, 10),
        'product_id' => random_int(1, 10),
        'attribute' => 'normal',
        'quantity' => 10,
        'unit_price' => 100,
        'sub_total' => 200
    ];
});
