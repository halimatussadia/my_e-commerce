<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\BusinessSetting::class, function (Faker $faker) {
    return [
        'company_name' => 'E-commerce',
        'address' => 'Uttara, Dhaka 1229',
        'email' => 'example@gmail.com',
        'phone_number' => '+8801628644184',
        'web_address' => 'https://www.example.com',
        'hot_line' => '+8801727142766',
        'facebook' => 'https://fb.com/facebook.com',
        'twitter' => 'https://www.twitter.com',
        'instagram' => 'https://www.instagram.com',
        'pinterest' => 'https://www.pinterest.com',
        'youtube' => 'https://www.youtube.com',
        'about' => $faker->text,
        'terms_and_conditions' => $faker->text,
        'tag_line' => 'Shopping the way you like it !!!',
        'delivery_charge' => 60.00
    ];
});
