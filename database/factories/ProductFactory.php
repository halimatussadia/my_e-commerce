<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Product::class, function (Faker $faker) {
    return [
        'category_id' => random_int(1, 9),
        'brand_id' => random_int(1, 9),
        'attribute_id' =>1,
        'title' => $faker->title,
        'description' => $faker->realText(),
        'unit_price' => 10.00,
        'discount' => 10,
        'color_id' => 1,
        'status' => 1,
        'is_featured' => 1,
    ];
});
