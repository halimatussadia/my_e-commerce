<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use Faker\Generator as Faker;

$factory->define(\App\Models\Order::class, function (Faker $faker) {
    return [
        'user_id' => $faker->numberBetween(1, 9),
        'order_number' => $faker->randomDigit,
        'delivery_charge' => 60,
        'discount_price' => 0,
        'total' => 1000,
        'receiver_name' => $faker->name,
        'email' => $faker->safeEmail,
        'receiver_phone' => $faker->phoneNumber,
        'receiver_address' => $faker->address,
        'status' => 0
    ];
});
