<?php

/** @var \Illuminate\Database\Eloquent\Factory $factory */

use App\Model;
use Faker\Generator as Faker;

$factory->define(\App\Models\Blog::class, function (Faker $faker) {
    return [
        'title' =>$faker->realText(32),
        'description' =>$faker->realText(32),
        'status' =>1,
    ];
});
