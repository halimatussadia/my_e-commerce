<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateUsersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('users', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->string('name', 96);
            $table->string('email', 64)->unique();
            $table->text('address');
            $table->string('phone_number', 32)->unique();
            $table->tinyInteger('email_verified')->default(0);
            $table->string('email_verified_at')->nullable();
            $table->string('email_verification_token')->nullable();
            $table->string('password',196);
            $table->string('role', 32)->default('user');
            $table->string('image')->default('default_img.png');
            $table->tinyInteger('status')->default(1);
            $table->rememberToken();
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('users');
    }
}
