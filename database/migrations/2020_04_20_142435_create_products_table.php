<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateProductsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('products', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->unsignedBigInteger('brand_id')->nullable();
            $table->unsignedBigInteger('category_id')->unsigned();
            $table->text('attribute_id')->nullable();
            $table->text('color_id')->nullable();
            $table->string('title', 64);
            $table->string('code', 64)->nullable();
            $table->text('image')->nullable();
            $table->text('description');
            $table->decimal('unit_price', 10, 2);
            $table->decimal('discount')->nullable()->default(0);
            $table->tinyInteger('status')->default(1);
            $table->tinyInteger('is_featured')->nullable()->default(0);
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('products');
    }
}
