<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateOrdersTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('orders', function (Blueprint $table) {
            $table->bigIncrements('id');
            $table->integer('user_id')->unsigned();
            $table->string('order_number',64);
            $table->decimal('delivery_charge')->nullable();
            $table->decimal('discount_price')->nullable()->default('0');
            $table->decimal('total');
            $table->string('receiver_name');
            $table->string('email')->nullable();
            $table->string('receiver_phone');
            $table->text('receiver_address');
            $table->String('status')->default('pending');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('orders');
    }
}
