<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Str;
use App\Models\User;

class UsersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        User::create([
            'name' => 'admin',
            'phone_number' => '01770146179',
            'address' => 'uttara',
            'role' => 'admin',
            'email' => 'admin@gmail.com',
            'email_verified' => 1,
            'email_verified_at' => \Carbon\Carbon::now(),
            'email_verification_token' => Str::random(30),
            'password' => bcrypt('admin')
        ]);
        factory(User::class, 5)->create();
    }
}
