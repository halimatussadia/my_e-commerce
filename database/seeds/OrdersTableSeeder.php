<?php

use App\Models\Order;
use App\Models\OrderDetails;
use Illuminate\Database\Seeder;

class OrdersTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        // Create 10 records of customers
        factory(Order::class, 5)->create()->each(function ($orderDetails) {
            // Seed the relation with one address
            $details = factory(OrderDetails::class)->make();
            $orderDetails->orderDetails()->save($details);

        });
    }
}


