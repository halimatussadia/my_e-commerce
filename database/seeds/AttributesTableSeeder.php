<?php

use Illuminate\Database\Seeder;

class AttributesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        \App\Models\Attribute::create([
            'size' => 'Normal',
            'code' => 0000,
        ]);
    }
}
