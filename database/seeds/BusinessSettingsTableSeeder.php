<?php

use Illuminate\Database\Seeder;

class BusinessSettingsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        factory(\App\Models\BusinessSetting::class,1)->create();
    }
}
