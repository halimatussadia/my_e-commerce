<?php

namespace App\Providers;

use App\Models\BusinessSetting;
use App\Models\Category;
use App\Models\Department;
use App\Models\Order;
use App\Models\Slider;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Schema;
use Illuminate\Support\Facades\View;
use Illuminate\Support\ServiceProvider;

class AppServiceProvider extends ServiceProvider
{
    /**
     * Register any application services.
     *
     * @return void
     */
    public function register()
    {
        //
    }

    /**
     * Bootstrap any application services.
     *
     * @return void
     */
    public function boot()
    {
        if (Schema::hasTable('business_settings')) {
            $business = BusinessSetting::first();
            View::share('business', $business);
        }
        if (Schema::hasTable('categories')) {
            $categories = Category::with('products')->where('status',1)->take(8)->get();
            View::share('categories', $categories);
        }

        if (Schema::hasTable('departments')) {
            $departments = Department::with('category')->where('status',1)->take(8)->get();
            View::share('departments', $departments);
        }
        if (Schema::hasTable('sliders')) {
            $sliders = Slider::orderBy('id', 'desc')->where('status', 1)->latest('id')->first();
            View::share('sliders', $sliders);
        }

        if(Schema::hasTable('orders')){

            $current_user = auth()->user()->id ?? 1;
           $totals= Order::where('user_id', '=', $current_user)
               ->selectRaw('count(*) as total')
               ->selectRaw("count(case when status = 'pending' then 1 end) as pending")
               ->selectRaw("count(case when status = 'processing' then 1 end) as processing")
               ->selectRaw("count(case when status = 'confirmed' then 1 end) as confirmed")
               ->selectRaw("count(case when status = 'received' then 1 end) as received")
               ->selectRaw("count(case when status = 'delivered' then 1 end) as delivered")
               ->selectRaw("count(case when status = 'canceled' then 1 end) as canceled")
               ->first();
           View::share('totals',$totals);
        }
    }
}
