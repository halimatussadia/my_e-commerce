<?php

namespace App\Http\Middleware;

use Brian2694\Toastr\Toastr;
use Closure;
use Illuminate\Support\Facades\Auth;

class IsAdmin
{
    /**
     * Handle an incoming request.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \Closure  $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {
        if(Auth::user()->role !== 'customer')
        {
            return $next($request);
        }
        return redirect()->route('login');
    }
}
