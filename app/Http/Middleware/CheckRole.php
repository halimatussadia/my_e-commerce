<?php

namespace App\Http\Middleware;

use Closure;

class CheckRole
{
    /**
     * Handle an incoming request.
     *
     * @param \Illuminate\Http\Request $request
     * @param \Closure $next
     * @return mixed
     */
    public function handle($request, Closure $next)
    {

        if (auth()->check() === true && auth()->user()->role === 'admin') {
            return $next($request);
        }
        auth()->logout();
        session()->flush();
        return route('login');
    }
}
