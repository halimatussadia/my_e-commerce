<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;

class CategoryController extends Controller
{
    public function index()
    {
        return view('frontend.product.products',[
            'products' => Product::orderBy('id', 'desc')->where('is_featured', 1)->paginate(12),
            'categories' => Category::with('products')->where('status', 1)->paginate(12),
        ]);
    }

    public function categoryWiseProduct($id)
    {
        $products=Product::where('category_id',$id)->orderBy('id', 'desc')->paginate(9);
        return view('frontend.product.products ',compact('products'));
    }
}
