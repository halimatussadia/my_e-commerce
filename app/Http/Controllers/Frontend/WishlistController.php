<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Product;
use App\Models\Wishlist;
use Illuminate\Http\Request;

class WishlistController extends Controller
{
    public function index()
    {
        $wishlists = Wishlist::where('user_id',auth()->user()->id)->with('products')->orderBy('id','desc')->get();
        return view('frontend.wishlist.index',compact('wishlists'));
    }

    public  function addToWishlist($id)
    {
        $product = Product::findorfail($id);
        Wishlist::create([
            'product_id' => $product->id,
            'user_id'    => auth()->user()->id,
        ]);

        notify('success','Product successfully add to wishlist....!');
        return redirect()->back();
    }

    public function delete($id)
    {
        Wishlist::find($id)->delete();
        notify('success','Product successfully deleted to wishlist....!');
        return redirect()->back();

    }
}
