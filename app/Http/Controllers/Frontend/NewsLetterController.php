<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\NewsLetter;
use Illuminate\Http\Request;

class NewsLetterController extends Controller
{
    public function create(Request $request)
    {
        NewsLetter::create([
            'email'=>$request->input('email'),
        ]);
        notify('success', 'Thank you for subscription...!');
        return redirect()->back();
    }
}
