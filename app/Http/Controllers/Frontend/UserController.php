<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\User;
use Carbon\Carbon;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class UserController extends Controller
{
    public function profile()
    {
        $search = request('order_number');
        $current_user = auth()->user()->id;
        $orders = Order::where('user_id',$current_user)->where('order_number', 'LIKE', $search)->orderBy('id','desc')->paginate(4);
        $new_orders = Order::where('user_id', $current_user)
            ->whereDay('created_at',Carbon::today())
            ->orderBy('created_at','desc')->paginate(4);

        return view('frontend.profile.index', [
            'orders'=>$orders,
            'profiles'=>auth()->user(),
            'new_orders'=>$new_orders,
            'totals'=>Order::getTotals($current_user),
            'status'=>Order::humanReadableStatus()
        ]);
    }

    public function update(Request $request, $id)
    {
        $validator = Validator::make($request->all(), [
                'name' => 'required|max:196',
                'email' => 'email|required|max:96',
                'phone_number' => 'required|min:11|max:16',
            ]
        );
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        $photo = $request->file('image');
        if ($photo) {
            $file_name = uniqid('image', true) . time() . '.' . $photo->getClientOriginalExtension();
            $photo->move(public_path('/uploads/users'), $file_name);
            $user = [
                'name' => $request->input('name'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
                'phone_number' => $request->input('phone_number'),
                'image' => $file_name
            ];
            User::where('id', $id)->update($user);
            notify('success', 'Your Profile Updated Successfully');
            return redirect()->back();
        } else {
            $user = [
                'name' => $request->input('name'),
                'phone_number' => $request->input('phone_number'),
                'email' => $request->input('email'),
                'address' => $request->input('address'),
            ];
            User::where('id', $id)->update($user);
            notify('success', 'Your Profile Updated Successfully');
            return redirect()->back();
        }

    }

    public function details($id)
    {
        $order = Order::with(['user', 'orderDetails.product'])->where('id', $id)->first();
        return view('frontend.profile.details', compact( 'order'));

    }

}
