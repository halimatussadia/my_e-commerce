<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use App\Notifications\ForgotPassword;
use Brian2694\Toastr\Facades\Toastr;
use Illuminate\Http\Request;
use Illuminate\Support\Carbon;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Str;
use Illuminate\Validation\ValidationException;

class ForgotPasswordController extends Controller
{
    public function forgot()
    {
        return view('frontend.auth.forgotPassword');
    }

    public function password(Request $request)
    {
        try {
            $this->validate($request, [
                'email' => 'required|email'
            ]);
        } catch (ValidationException $exception) {
            return redirect()->back();
        }

        $user = User::where('email', '=', $request->input('email'))->first();

        if ($user === null) {
            return redirect()->back();
        }
        DB::table('password_resets')->insert([
            'email' => $request->email,
            'token' => Str::random(60),
            'created_at' => Carbon::now()
        ]);
        $tokenData = DB::table('password_resets')
            ->where('email', $request->email)->first();
        if ($this->sendResetEmail($request->email, $tokenData->token)) {
            notify('error','A Network Error occurred. Please try again.');
            return redirect()->back();
        } else {
            notify('success','A reset link has been sent to your email address.');
            return redirect()->back();
        }

    }

    private function sendResetEmail($email, $token)
    {
        $user = User::where('email', $email)->select('email')->first();
        $user->notify(new ForgotPassword($user, $token));

    }

    public function showResetForm($token)
    {
        $tokenVerify = DB::table('password_resets')->where('token', $token)->first();
        if ($tokenVerify === null) {
            return redirect()->route('forgot');
        }
        return view('frontend.auth.reset', compact('token'));

    }

    public function reset(Request $request, $token)
    {
        $tokenVerify = DB::table('password_resets')->where('token', $token)->first();
        if ($tokenVerify->token == $token) {
            $user = User::where('email', $tokenVerify->email)->first();
            $user->update([
                'password' => app('hash')->make($request->input('password')),
            ]);
            $tokenVerify = DB::table('password_resets')->where('email', $user->email)->delete();

            notify('success','Password updated successfully.');
            return redirect()->route('registration');
        }
        notify('success','Incorrect password.');
        return redirect()->back();
    }
}
