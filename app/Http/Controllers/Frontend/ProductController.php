<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use Illuminate\Http\Request;

class ProductController extends Controller
{
    public function index()
    {
        return view('frontend.product.products',[
            'products' => Product::orderBy('id', 'desc')->where('status', 1)->paginate(12),
            'categories' => Category::with('products')->where('status', 1)->paginate(12),
        ]);
    }

    public function details($id)
    {
        $products = Product::with('stock')->findorfail($id);
        $attributes = Attribute::findMany(json_decode($products->attribute_id));
        $colors = Color::findMany(json_decode($products->color_id));
        $relatedProducts = Product::where('category_id', $products->category_id)->where('status', '=', 1)->take(4)->get();
        $brand = Product::where('brand_id', $products->brand_id)->where('status', '=', 1)->get();
        return view('frontend.product.product_detail',compact('products','relatedProducts','attributes'
        ,'brand','colors'));
    }


}
