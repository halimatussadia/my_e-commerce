<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Validator;
use Illuminate\Support\Str;

class AuthController extends Controller
{
    public function index()
    {
        return view('frontend.auth.register');
    }

    public function create(Request $request)
    {
        $validator = Validator::make($request->all(), [
            'name' => 'required|max:250|',
            'email' => 'required|unique:users,email|email',
            'password' => 'required|min:6',
            'phone_number' => 'required|max:16|unique:users,phone_number|min:11|max:16',
            'address'=>'required'
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }

        $photo = $request->file('image');
        $file_name = '';
        if ($photo) {
            $file_name = uniqid('image_', true) . time() . '.' . $photo->getClientOriginalExtension();
            $photo->move(public_path('/uploads/users'), $file_name);

        }

        $data = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'password' => app('hash')->make($request->input('password')),
            'phone_number' => $request->input('phone_number'),
            'address' => $request->input('address'),
            'role' => 'customer',
            'image' => $file_name,
            'email_verified_at' => trim($request->input('email_verified_at')),
            'email_verification_token' => str::random(30),
        ];
        $user = User::create($data);
        notify('success', 'Registration successful.');
        return redirect()->route('registration');

    }

    public function processLogin(Request $request)
    {
        $this->validate($request,[
            'email'=>'required|email',
            'password'=>'required'
        ]);
        $credentials = [
            'email'=>$request->input('email'),
            'password'=>$request->input('password')
        ];

        if (Auth::guard('web')->attempt($credentials)) {
            notify('success', 'Login successfully');

            return redirect()->route('home.index');

        } else {
            notify('success', 'Invalid credentials.');
            return redirect()->back();
        }

    }

    public function logout()
    {
        Auth::logout();
        session()->invalidate();
        notify('success', 'Logout successfully.');
        return redirect()->route('registration');
    }
}
