<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\OrderDetails;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\Validator;

class OrderController extends Controller
{
    public function index()
    {
        $carts = session('cart') ?? [];
        $total = array_sum(array_column($carts, 'total_price'));
        $total_discount = array_sum(array_column($carts, 'total_discount'));
        $user = User::where('id', auth()->user()->id)->first();
        return view('frontend.checkout.index', compact('carts', 'total_discount', 'total', 'user'));
    }

    public function create(Request $request)
    {
        $carts = session('cart');
        if ($carts == null) {
            notify('error', 'Your cart is empty.');
            return redirect()->back();
        }
        $validator = Validator::make($request->all(), [
            'email' => 'email',
            'receiver_name' => 'required|min:2|regex:/[A-Za-z. -]/|max:255',
            'receiver_phone' => 'required|regex:/^([0-9\s\-\+\(\)]*)$/|min:11',
            'receiver_address' => 'required',
        ]);
        if ($validator->fails()) {
            return redirect()->back()
                ->withErrors($validator)
                ->withInput();
        }
        Db::beginTransaction();
        try {
            $user = auth()->user()->id;
            $order = Order::create([
                'user_id' => $user,
                'order_number' => date('Ymdhms'),
                'total' => $request->input('total'),
                'receiver_name' => $request->input('receiver_name'),
                'email' => $request->input('email'),
                'receiver_phone' => $request->input('receiver_phone'),
                'receiver_address' => $request->input('receiver_address'),
                'discount_price' => $request->input('discount_price'),
                'delivery_charge' => $request->input('delivery_charge'),
            ]);
            $carts = session('cart');

            foreach ($carts as $cart) {
                $orderDetails = OrderDetails::create([
                    'order_id' => $order->id,
                    'product_id' => $cart['product_id'],
                    'quantity' => $cart['quantity'],
                    'unit_price' => $cart['unit_price'],
                    'sub_total' => $cart['total_price'],
                    'attribute' => $cart['options'] ?? null,
                    'color' => $cart['color'] ?? null
                ]);
            }

            foreach ($order->orderDetails as $detail) {
                $stock = ($detail->product->stock->quantity - $detail->quantity);
                $detail->product->stock->quantity = $stock;
                $detail->product->stock->save();
            }

            if ($order) {
                session()->forget('cart');
            }
            DB::commit();
            notify('success', 'Thank you for you Order');
            return redirect()->route('home.index');
        } catch (\Exception $exception) {
            DB::rollBack();
            error('error', 'There are some problem');
            return redirect()->back();
        }


    }
}
