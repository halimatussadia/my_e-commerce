<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Color;
use App\Models\Product;
use Illuminate\Http\Request;

class CartController extends Controller
{
    public function index()
    {
        return view('frontend.cart.index');
    }

    public function addToCart(Request $request, $id)
    {
        $product = Product::findorfail($id);
        if ($request->size !== null) {
            $attribute = Attribute::find($request->size);
        }
        if ($request->name !== null) {
            $color = Color::find($request->name);
        }
        $cart = session()->has('cart') ? session()->get('cart') : [];
        if (array_key_exists($product->id, $cart)) {

            $cart[$product->id]['quantity']++;
            $cart[$product->id]['total_price'] = ((int)$cart[$product->id]['quantity'] * (float)$cart[$product->id]['unit_price'])
                - ((integer)$cart[$product->id]['quantity'] * (($product->unit_price) * ($product->discount / 100)));
        } else {
            $cart[$product->id] = [
                'options' => $attribute->size ?? null,
                'color' => $color->name ?? null,
                'product_id' => $product->id,
                'title' => $product->title,
                'quantity' => 1,
                'unit_price' => $product->unit_price,
                'total_discount' => $product->discount,
                'discount' => $product->discount,
                'total_price' => ((float)$product->unit_price) - (((float)$product->unit_price) * ((float)$product->discount / 100)),
            ];
        }

        session(['cart' => $cart]);
        notify('success', 'Product added successfully...!');
        return redirect()->route('home.index');
    }

    function updateCart(Request $request)
    {
        $id = $request->input('cart_id');
        $cart = \session()->get('cart');
        $cart[$id]['quantity'] = $request->input('quantity');
        $cart[$id]['total_price'] = ($cart[$id]['quantity'] * $cart[$id]['unit_price']) -
            (((int)$cart[$id]['quantity'] * (((float)$cart[$id]['unit_price']) * ((float)$cart[$id]['discount'] / 100))));
        \session()->put('cart', $cart);
        notify('success', 'Cart Updated successful...!');
        return redirect()->route('cart.index');
    }

    public
    function delete($id)
    {
        $cart = \session('cart');
        unset($cart[$id]);
        \session()->put('cart', $cart);
        notify('success', 'Cart deleted successfully...!');
        return redirect()->back();
    }

    public
    function destroy()
    {
        \session()->forget('cart');
        notify('success', 'Cart cleared successfully...!');
        return redirect()->back();
    }
}
