<?php

namespace App\Http\Controllers\Frontend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use App\Models\Product;
use App\Models\Slider;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('frontend.home.index', [
            'products' => Product::orderBy('id', 'desc')->where('status', 1)->take(8)->get(),
            'feature_products' => Product::orderBy('id', 'desc')->where('status', 1)->where('is_featured', 1)->take(2)->get(),
            'new_products' => Product::where('status', 1)->where('is_featured', 1)->first(),
            'sliders' => Slider::orderBy('id', 'desc')->where('status', 1)->take(3)->get()
        ]);

    }
}
