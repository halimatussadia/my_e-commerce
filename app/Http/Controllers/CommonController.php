<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class CommonController extends Controller
{
    public function not_found()
    {
        return view('not_found');
    }

    public  function errorPage()
    {
        return view('server');
    }
}
