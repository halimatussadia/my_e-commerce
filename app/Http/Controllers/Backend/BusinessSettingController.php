<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\BusinessSetting;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BusinessSettingController extends Controller
{
    public  function  edit($id)
    {
        $edit = BusinessSetting::where('id',$id)->first();
        return view('backend.setting.edit',compact('edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'logo' => 'mimes:jpeg,png,jpg,gif|max:2048',
            'company_name' => 'required',
            'email' => 'email',
            'phone_number' => 'required|regex:/[0-9]/',
            'delivery_charge' => 'numeric',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

       $setting = BusinessSetting::where('id',$id)->first();
        $data = $request->except('_token');

        if($request->hasFile('logo')){
            $fileName = $request->file('logo');
            $photo = uniqid('photo_',true) . '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/setting'),$photo);
            if($setting->logo != 'default_img.png' && File::exists(public_path('uploads/setting/'.$setting->logo))){
                unlink(public_path('uploads/setting/'.$setting->logo));
            }
            $data['logo'] = $photo;
        }

        try{
            $setting->update($data);
            notify('success', 'Business Setting updated successfully...!');
            return redirect()->back();
        }catch (\Exception $exception){
            emotify('error', 'Business Setting not update please check again...!!');
            return redirect()->back();
        }
    }
}
