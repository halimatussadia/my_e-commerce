<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Brand;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BrandController extends Controller
{
    public function index()
    {
        $brands = Brand::orderBy('id','desc')->get();
        return view('backend.brand.index',compact('brands'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required|unique:brands,name',
            'description' => 'string',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
        ];

        $fileName = $request->file('image');
        if($fileName){
            $photo = uniqid('photo_',true) .'.'.$fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/brand'),$photo);
            $data['image'] = $photo;
        }
        try{
            Brand::create($data);
            notify('success', 'Brand created successful...!');
            return redirect()->back();
        }catch (\Exception $exception){
            emotify('error', 'Brand not created please check again...!!');
            return redirect()->back();
        }
    }

    public function  edit($id)
    {
        $edit = Brand::findorfail($id);
        return view('backend.brand.edit',compact('edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required',
            'description' => 'string',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
        ];

        $brand = Brand::where('id',$id)->first();

        if($request->hasFile('image')){
           $fileName = $request->file('image');
           $photo = uniqid('photo_',true) . '.' . $fileName->getClientOriginalName();
           $fileName->move(public_path('uploads/brand'),$photo);

           if($brand->image != 'default_img.png' && File::exists(public_path('uploads/brand/'.$brand->image))){
               unlink(public_path('uploads/brand/'.$brand->image));
           }
           $data['image'] = $photo;
        }
        try{
            $brand->update($data);
            notify('success', 'Brand updated successfully...!');
            return redirect()->route('brands.index');
        }catch (\Exception $exception){
            emotify('error', 'Brand not update please check again...!!');
            return redirect()->back();
        }
    }
}
