<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use Illuminate\Http\Request;

class AttributeController extends Controller
{
    public function index()
    {
        $attributes = Attribute::orderBy('id','desc')->get();
        return view('backend.attribute.index',compact('attributes'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'size' => 'required|unique:attributes,size',
            'code' => 'string|unique:attributes,code',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'size' => $request->input('size'),
            'code' => $request->input('code'),
        ];
        try{
            Attribute::create($data);
            notify('success', 'Attribute created successful...!');
            return redirect()->back();
        }catch (\Exception $exception){
            emotify('error', 'Attribute not created please check again...!!');
            return redirect()->back();
        }
    }

    public function  edit($id)
    {
        $edit = Attribute::findorfail($id);
        return view('backend.attribute.edit',compact('edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'size' => 'required',
            'code' => 'string',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'size' => $request->input('size'),
            'code' => $request->input('code'),
            'status' => $request->input('status'),
        ];
        try{
            Attribute::where('id',$id)->update($data);
            notify('success', 'Attribute updated successfully...!');
            return redirect()->route('attribute.index');
        }catch (\Exception $exception){
            emotify('error', 'Attribute not update please check again...!!');
            return redirect()->back();
        }
    }
}
