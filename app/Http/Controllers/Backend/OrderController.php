<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use Illuminate\Http\Request;

class OrderController extends Controller
{
    public function index($status = null)
    {
        $getStatus = Order::getStatus();
        $orders = Order::with('user')->orderBy('id', 'desc')->get();
        if ($status!=null){
            $orders = Order::with('user')->orderBy('id', 'desc')->where('status',$status)->get();
        }
        return view('backend.order.index', compact('orders','status','getStatus'));
    }

    public function orderDetails($id)
    {
        $order = Order::with(['user', 'orderDetails.product'])->where('id', $id)->first();
        return view('backend.order.order_details',compact('order'));

    }
    public function invoice($id)
    {
        $order = Order::with(['user', 'orderDetails.product'])->where('id', $id)->first();
        return view('backend.order.order_invoice',compact('order'));

    }


    public function status($id, String $statusCode)
    {
        $validates_code = [
            Order::PENDING => "Order successfully pending",
            Order::PROCESSING => "Order successfully processing",
            Order::CONFIRMED => "Order successfully confirmed",
            Order::RECEIVED => "Order successfully received",
            Order::DELIVERED => "Order successfully delivered",
            Order::CANCELED => "Order successfully canceled",
        ];

        if (array_key_exists($statusCode,$validates_code)) {
            Order::find($id)->update(['status' => $statusCode]);
            notify($validates_code[$statusCode]);
            return redirect()->back();
        }
    }

}
