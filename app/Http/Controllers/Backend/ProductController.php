<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Attribute;
use App\Models\Color;
use App\Models\Stock;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;
use Illuminate\Support\Facades\File;
use PHPUnit\Exception;

class ProductController extends Controller
{
    public function index()
    {
        $products = Product::with('stock')->orderBy('id','desc')->get();
        return view('backend.product.index',compact('products'));
    }

    public function create()
    {
        $categories = Category::where('status',1)->orderBy('id','desc')->get();
        $brands = Brand::where('status',1)->orderBy('id','desc')->get();
        $attributes = Attribute::where('status',1)->orderBy('id','desc')->get();
        $colors = Color::where('status',1)->orderBy('id','desc')->get();
        return view('backend.product.create',compact('categories','brands','attributes','colors'));
    }


    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'title' => 'required|unique:products,title',
            'unit_price' => 'required|regex:/^\d+(\.\d{1,2})?$/',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $products = [
            'title'          => $request['title'],
            'code'          => $request['code'],
            'category_id'    => $request['category_id'],
            'brand_id'       => $request['brand_id'],
            'attribute_id'   => json_encode($request['attribute_id']),
            'color_id'       => json_encode($request['color_id']),
            'unit_price'     => $request['unit_price'],
            'discount'       => $request['discount'],
            'description'    => $request['description'],
        ];

        if($request->hasfile('image')){
            foreach ($request->file('image') as $key=>$file){
                $photo = uniqid('photo_',true) . '.' . $file->getClientOriginalName();
                $file->move(public_path('uploads/product'),$photo);
                $fileNames[] = $photo;
            }
        }
        $products['image'] = json_encode($fileNames) ?? null;
        try{
            DB::beginTransaction();
           $product = Product::create($products);
           $stock = Stock::create([
                'product_id' => $product->id,
                'quantity' => $request->input('quantity'),
            ]);
            DB::commit();
            notify('success','Product created successfully..!');
            return redirect()->route('products.index');
        }catch (Exception $exception){
            DB::rollBack();
            notify('error','Product is not create successfully please check again..!');
            return redirect()->back();
        }
    }

    public function details($id)
    {
        $product = Product::with('stock')->findorfail($id);
        $attributes = Attribute::findMany(json_decode($product->attribute_id));
        $colors = Color::findMany(json_decode($product->color_id));
        return view('backend.product.details',compact('product','attributes','colors'));
    }

    public function edit($id)
    {
        $edit = Product::with('stock')->findorfail($id);
        $categories = Category::where('status',1)->orderBy('id','desc')->get();
        $brands = Brand::where('status',1)->orderBy('id','desc')->get();
        $attributes = Attribute::findMany(json_decode($edit->attribute_id));
        $colors = Color::findMany(json_decode($edit->color_id));
        return view('backend.product.edit',compact('categories','brands','edit','attributes','colors'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'unit_price' => 'regex:/^\d+(\.\d{1,2})?$/',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $products = [
            'title'          => $request['title'],
            'code'          => $request['code'],
            'category_id'    => $request['category_id'],
            'brand_id'       => $request['brand_id'],
            'attribute_id'   => json_encode($request['attribute_id']),
            'color_id'       => json_encode($request['color_id']),
            'unit_price'     => $request['unit_price'],
            'discount'       => $request['discount'],
            'description'    => $request['description'],
            'status'         => $request['status'],
            'is_featured'    => $request['is_featured'],
        ];
        DB::beginTransaction();

        try{
            $product = Product::find($id);
            if($request->hasfile('image')){
                foreach ($request->file('image') as $key=>$file){
                    $photo = uniqid('photo_',true) . '.' . $file->getClientOriginalName();
                    $file->move(public_path('uploads/product'),$photo);
                    $fileNames[] = $photo;
                }

                $products['image'] = json_encode($fileNames??[]) ?? null;
                $product->update($products);
            }
            unset($products['image']);
            $product->update($products);
            $stock = Stock::where('id',$id)->update([
                'product_id' => $product->id,
                'quantity' => $request->input('quantity'),
            ]);
            DB::commit();
            notify('success','Product updated successfully..!');
            return redirect()->route('products.index');
        }catch (\Throwable $throwable){
            DB::rollBack();
            notify('error','Product is not update successfully please check again..!');
            return redirect()->back();
        }



    }

}
