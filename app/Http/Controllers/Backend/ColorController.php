<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Color;
use Illuminate\Http\Request;

class ColorController extends Controller
{
    public function index()
    {
        $colors = Color::orderBy('id','desc')->get();
        return view('backend.color.index',compact('colors'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required|unique:colors,name',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'name' => $request->input('name'),
        ];
        try{
            Color::create($data);
            notify('success', 'Color created successful...!');
            return redirect()->back();
        }catch (\Exception $exception){
            emotify('error', 'Color not created please check again...!!');
            return redirect()->back();
        }
    }

    public function  edit($id)
    {
        $edit = Color::findorfail($id);
        return view('backend.color.edit',compact('edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required',
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'name' => $request->input('name'),
            'status' => $request->input('status'),
        ];
        try{
            Color::where('id',$id)->update($data);
            notify('success', 'Color updated successfully...!');
            return redirect()->route('color.index');
        }catch (\Exception $exception){
            emotify('error', 'Color not update please check again...!!');
            return redirect()->back();
        }
    }
}
