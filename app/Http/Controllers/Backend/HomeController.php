<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Order;
use App\Models\Product;
use Illuminate\Http\Request;

class HomeController extends Controller
{
    public function index()
    {
        return view('backend.home.index',[
            'totalOrder' => Order::count('id'),
            'totalEarinig' => Order::where('status', '3')->sum('total'),
            'pendingOrder' => Order::where('status', '0')->count(),
            'totalProduct' => Product::where('status', '1')->count(),
        ]);
    }
}
