<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    public function login()
    {
        return view('backend.login.index');
    }

    public function processLogin(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|email',
            'password' => 'required',
        ]);

        $credentials = [
            'email' => $request->input('email'),
            'password' => $request->input('password'),
        ];
        if (Auth::attempt($credentials)) {
            if (\auth()->user()->role !== 'admin') {
                emotify('success', 'You are logged in...!');
                return redirect()->route('home.index');
            } else {
                emotify('success', 'You are logged in...!');
                return redirect()->route('home');
            }
        }
        emotify('error', 'Invalid credential...!!');
        return redirect()->back();
    }

    public function logout()
    {
        emotify('success', 'You are logged out...!');
        Auth::logout();
        return redirect()->route('login');
    }
}
