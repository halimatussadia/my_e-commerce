<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class BlogController extends Controller
{
    public  function  index()
    {
        $blogs = Blog::orderBy('id','desc')->get();
        return view('backend.blog.index',compact('blogs'));
    }

    public function create()
    {
        return view('backend.blog.create');
    }

    public function  store(Request $request)
    {
        $validation = \Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'string',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if($validation->fails())
        {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $data= [
            'title' => $request->input('title'),
            'description' => $request->input('description'),
        ];

        $fileName = $request->file('image');
        if($fileName){
            $photo = uniqid('photo_',true) .'.'.$fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/blog'),$photo);
            $data['image'] = $photo;
        }
        try{
            Blog::create($data);
            notify('success', 'Blog created successfully...!');
            return redirect()->back();
        }catch (\Exception $exception){
            emotify('error', 'Blog not created please check again...!!');
            return redirect()->back();
        }

    }

    public  function  edit($id)
    {
        $blog_edit = Blog::findorfail($id);
        return view('backend.blog.edit',compact('blog_edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'title' => 'required',
            'description' => 'string',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);


        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $data = [
            'title' => $request->input('title'),
            'description' => $request->input('description'),
            'status' => $request->input('status'),
        ];

        $blog = Blog::where('id',$id)->first();

        if($request->hasFile('image')){
            $fileName = $request->file('image');
            $photo = uniqid('photo_',true) . '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/brand'),$photo);

            if($blog->image != 'default_img.png' && File::exists(public_path('uploads/blog/'.$blog->image))){
                unlink(public_path('uploads/blog/'.$blog->image));
            }
            $data['image'] = $photo;
        }
        try{
            $blog->update($data);
            notify('success', 'Blog updated successfully...!');
            return redirect()->route('brands.index');
        }catch (\Exception $exception){
            emotify('error', 'Blog not update please check again...!!');
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        try{
            Blog::findorfail($id)->delete();
            notify('success','Blog delete Successful...!!');
            return redirect()->back();
        }catch (\Exception $exception){
            notify('success','Blog delete Successful...!!');
            return redirect()->back();
        }

    }
}
