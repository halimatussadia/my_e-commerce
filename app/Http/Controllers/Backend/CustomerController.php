<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;

class CustomerController extends Controller
{
    public function index()
    {
        $customers = User::whereNotIn('id',[1])->orderBy('id','desc')->get();
        return view('backend.customer.index',compact('customers'));
    }

    public function delete($id)
    {
        try{
            User::findorfail($id)->delete();
            notify('success','Customer delete Successful...!!');
            return redirect()->back();
        }catch (\Exception $exception){
            notify('success','Customer delete Successful...!!');
            return redirect()->back();
        }

    }
}
