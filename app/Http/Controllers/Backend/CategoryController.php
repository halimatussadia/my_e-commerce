<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Category;
use App\Models\Department;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;
use PHPUnit\Exception;

class CategoryController extends Controller
{
    public  function  index()
    {
        $categories = Category::with('department')->orderBy('id','desc')->get();
        $departments = Department::orderBy('id','desc')->where('status',1)->get();
        return view('backend.category.index',compact('categories','departments'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required|unique:categories,name',
            'description' => 'string',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $category = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'department_id' => $request->input('department_id'),
        ];

        $fileName = $request->file('image');
        if($fileName){
            $photo = uniqid('photo_',true) . '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/category'),$photo);
            $category['image'] = $photo;
        }
        try{
            Category::create($category);
            notify('success','Category created successfully');
            return redirect()->back();
        }catch (Exception $exception){
            notify('error','Category is not create successfully please check again');
            return redirect()->back();
        }
    }

    public  function edit($id)
    {
        $edit = Category::with('department')->findorfail($id);
        $departments = Department::orderBy('id','desc')->where('status',1)->get();
        return view('backend.category.edit',compact('edit','departments'));
    }

    public  function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required',
            'description' => 'string',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }

        $category = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'department_id' => $request->input('department_id'),
            'status' => $request->input('status'),
            'feature' => $request->input('feature'),
        ];

        $categories = Category::where('id',$id)->first();

        if($request->hasfile('image')){
            $fileName = $request->file('image');
            $file = uniqid('photo_',true).'.'.$fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/category'),$file);

            if($categories->image != 'default_img.png' && File::exists(public_path('uploads/category/'.$categories->image))){

                unlink(public_path('uploads/category/'.$categories->image));
            }
            $category['image'] = $file;
        }
        try{
            $categories->update($category);
            notify('success','Category updated successfully');
            return redirect()->route('categories.index');
        }catch (Exception $exception){
            notify('error','Category is not update successfully please check again');
            return redirect()->back();
        }
    }
}
