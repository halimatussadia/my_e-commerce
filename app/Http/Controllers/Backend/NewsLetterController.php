<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\NewsLetter;
use Illuminate\Http\Request;

class NewsLetterController extends Controller
{
    public function index()
    {
        $newsletters = NewsLetter::orderBy('id','desc')->get();
        return view('backend.newsletter.index',compact('newsletters'));
    }

    public function delete($id)
    {
        try{
            NewsLetter::findorfail($id)->delete();
            notify('success','NewsLetter delete Successful...!!');
            return redirect()->back();
        }catch (\Exception $exception){
            notify('success','NewsLetter delete Successful...!!');
            return redirect()->back();
        }

    }
}
