<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Blog;
use App\Models\Department;
use http\Exception;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class DepartmentController extends Controller
{
    public  function  index()
    {
        $departments = Department::orderBy('id','desc')->get();
        return view('backend.department.index',compact('departments'));
    }


    public function  store(Request $request)
    {
        $validation = \Validator::make($request->all(),[
            'name' => 'required',
            'description' => 'string',
            'code' => 'string',
        ]);

        if($validation->fails())
        {
            return redirect()->back()->withErrors($validation)->withInput();
        }

        $department= [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'code' => $request->input('code'),
            'uid' => $request->input('uid'),
        ];
        $fileName = $request->file('image');
        if($fileName){
            $photo = uniqid('photo_',true) . '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/department'),$photo);
            $department['image'] = $photo;
        }
        try{
            Department::create($department);
            notify('success', 'Department created successfully...!');
            return redirect()->back();
        }catch (\Exception $exception){
            emotify('error', 'Department not created please check again...!!');
            return redirect()->back();
        }
    }

    public  function  edit($id)
    {
        $edit = Department::findorfail($id);
        return view('backend.department.edit',compact('edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'name' => 'required',
            'description' => 'string',
            'image' => 'image|mimes:jpeg,png,jpg,gif|max:2048'
        ]);

        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $department = [
            'name' => $request->input('name'),
            'description' => $request->input('description'),
            'code' => $request->input('code'),
            'uid' => $request->input('uid'),
            'status' => $request->input('status'),
        ];

        $departments = Department::where('id',$id)->first();

        if($request->hasFile('image')){
            $fileName = $request->file('image');
            $photo = uniqid('photo_',true) . '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/department'),$photo);

            if($departments->image != 'default_img.png' && File::exists(public_path('uploads/department/'.$departments->image))){
                unlink(public_path('uploads/department/'.$departments->image));
            }
            $department['image'] = $photo;
        }
        try{
            $departments->update($department);
            notify('success', 'Department updated successfully...!');
            return redirect()->route('department.index');
        }catch (\Exception $exception){
            emotify('error', 'Department not update please check again...!!');
            return redirect()->back();
        }
    }

}
