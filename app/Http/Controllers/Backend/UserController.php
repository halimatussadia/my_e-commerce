<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class UserController extends Controller
{
    public function index()
    {
        if(auth()->user()->role == 'admin'){
            $employees = User::where('role', 'admin')->Orwhere('role', 'contentManager')
                ->Orwhere('role', 'salesManager')->Orwhere('role', 'designer')->orderBy('id', 'desc')->get();

        }else{
            $employees = User::where('id',auth()->user()->id)->orderBy('id', 'desc')->paginate(5);
        }
        return view('backend.employee.index',compact('employees'));
    }

    public function create()
    {
        return view('backend.employee.create');
    }

    public  function  store(Request $request)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:128|unique:users,name',
            'email' => 'email|required|max:64|unique:users,email',
            'password' => 'required',
            'phone_number' => 'required|max:32|unique:users,phone_number',
            'address' => 'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone_number' => $request->input('phone_number'),
            'address' => $request->input('address'),
            'password' => bcrypt($request->input('password')),
            'role' => $request->input('role'),
        ];

        $fileName = $request->file('image');
        if($fileName) {
            $photo = uniqid('photo_', true) . '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/user'), $photo);
            $user['image'] = $photo;
        }

        try{
            User::create($user);
            notify('success', 'User created successfully...!');
            return redirect()->route('employees.index');
        }catch (\Exception $exception){
            emotify('error', 'User not created please check again...!!');
            return redirect()->back();
        }

    }

    public function edit($id)
    {
        $edit = User::where('id',$id)->first();
        return view('backend.employee.edit',compact('edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(), [
            'name' => 'required|max:128',
            'email' => 'email|required|max:128',
            'password' => 'required',
            'phone_number' => 'required|max:32',
            'address' => 'required',
        ]);
        if($validator->fails()){
            return redirect()->back()->withErrors($validator)->withInput();
        }

        $user = [
            'name' => $request->input('name'),
            'email' => $request->input('email'),
            'phone_number' => $request->input('phone_number'),
            'address' => $request->input('address'),
            'password' => bcrypt($request->input('password')),
            'role' => $request->input('role'),
            'status' => $request->input('status'),
        ];

        $users = User::where('id',$id)->first();

        if($request->hasFile('image')){
            $fileName = $request->file('image');
            $photo = uniqid('photo_',true) . '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/user'), $photo);
            if($users->image != 'default_img.png' && File::exists(public_path('uploads/blog/'.$users->image))){

                unlink(public_path('uploads/blog/'.$users->image));
            }
            $user['image'] = $photo;
        }

        try{
            $users->update($user);
            notify('success', 'User updated successfully...!');
            return redirect()->route('employees.index');
        }catch (\Exception $exception){
            emotify('error', 'User not updated please check again...!!');
            return redirect()->back();
        }
    }
}
