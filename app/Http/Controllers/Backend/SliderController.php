<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Slider;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\File;

class SliderController extends Controller
{
    public function index()
    {
        $sliders = Slider::orderBy('id','desc')->get();
        return view('backend.slider.index',compact('sliders'));
    }

    public function store(Request $request)
    {
        $validator = \Validator::make($request->all(),[
            'slider_img' => 'image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $data = [
            'slider_heading' => $request->input('slider_heading'),
            'slider_description' => $request->input('slider_description'),
            'button_text' => $request->input('button_text'),
            'slider_url' => $request->input('slider_url'),
        ];

        $fileName = $request->file('slider_img');
        if($fileName){
           $photo = uniqid('photo_',true). '.' . $fileName->getClientOriginalName();
           $fileName->move(public_path('uploads/slider'),$photo);
           $data['slider_img'] = $photo;
        }

        try{
            Slider::create($data);
            notify('success','Slider Created Successful...!!');
            return redirect()->back();
        }catch (\Exception $exception){
            notify('error','Slider is not created successfully please check again...!!');
            return redirect()->back();
        }
    }

    public function edit($id)
    {
        $edit = Slider::findorfail($id);
        return view('backend.slider.edit',compact('edit'));
    }

    public function update(Request $request,$id)
    {
        $validator = \Validator::make($request->all(),[
            'slider_img' => 'image|mimes:jpg,png,jpeg,gif|max:2048',
        ]);
        if($validator->fails()){
            return redirect()->back()->withInput()->withErrors($validator);
        }
        $data = [
            'slider_heading' => $request->input('slider_heading'),
            'slider_description' => $request->input('slider_description'),
            'button_text' => $request->input('button_text'),
            'slider_url' => $request->input('slider_url'),
            'status' => $request->input('status'),
        ];

        $slider = Slider::where('id',$id)->first();

        if( $request->hasfile('slider_img')){
            $fileName = $request->file('slider_img');
            $photo = uniqid('photo_',true). '.' . $fileName->getClientOriginalName();
            $fileName->move(public_path('uploads/slider'),$photo);
            if($slider->slider_img != 'default_img.png' && File::exists(public_path('uploads/slider/'.$slider->slider_img))){
                unlink(public_path('uploads/slider/'.$slider->slider_img));
            }
            $data['slider_img'] = $photo;
        }
        try{
            $slider->update($data);
            notify('success','Slider updated Successful...!!');
            return redirect()->route('sliders.index');
        }catch (\Exception $exception){
            notify('error','Slider is not updated successfully please check again...!!');
            return redirect()->back();
        }
    }

    public function delete($id)
    {
        try{
            Slider::findorfail($id)->delete();
            notify('success','Slider delete Successful...!!');
            return redirect()->back();
        }catch (\Exception $exception){
            notify('success','Slider delete Successful...!!');
            return redirect()->back();
        }

    }
}
