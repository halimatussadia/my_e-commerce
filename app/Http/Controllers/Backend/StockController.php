<?php

namespace App\Http\Controllers\Backend;

use App\Http\Controllers\Controller;
use App\Models\Stock;
use Illuminate\Http\Request;

class StockController extends Controller
{
    public  function index()
    {
        $stocks = Stock::with('product')->orderBy('id','desc')->get();
        return view('backend.stock.index',compact('stocks'));
    }
}
