<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\DB;

class Order extends Model
{
    public const PENDING = "pending";
    public const PROCESSING = "processing";
    public const CONFIRMED = "confirmed";
    public const RECEIVED = "received";
    public const DELIVERED = "delivered";
    public const CANCELED = "canceled";

    protected $guarded = [];

    public function user()
    {
        return $this->belongsTo(User::class, 'user_id', 'id');
    }

    public function orderDetails()
    {
        return $this->hasMany(OrderDetails::class);
    }

    public static function getTotals($current_user)
    {
        return DB::table('orders')
            ->where('user_id', '=', $current_user)
            ->selectRaw('count(*) as total')
            ->selectRaw("count(case when status = 'pending' then 1 end) as pending")
            ->selectRaw("count(case when status = 'processing' then 1 end) as processing")
            ->selectRaw("count(case when status = 'confirmed' then 1 end) as confirmed")
            ->selectRaw("count(case when status = 'received' then 1 end) as received")
            ->selectRaw("count(case when status = 'delivered' then 1 end) as shipped")
            ->selectRaw("count(case when status = 'canceled' then 1 end) as delivered")
            ->first();

    }

    public static function getStatus()
    {
        return [
            "pending" => "badge-warning",
            "processing" => "badge-info",
            "confirmed" => "badge-primary",
            "received" => "badge-secondary",
            "delivered" => "badge-success",
            "canceled" => "badge-danger",
        ];
    }


    public static function humanReadableStatus()
    {
        return [
            "pending" => "Your has been placed successfully, we will contact you shortly",
            "processing" => "Your order on processing",
            "confirmed" => "Your order is confirmed",
            "received" => "Your order is Picking & Packaging for delivery",
            "delivered" => "Your order is successfully delivered",
            "canceled" => "Your order has been cancelled successfully",
        ];
    }


}
