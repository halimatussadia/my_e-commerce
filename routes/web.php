<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|-------------------------fsa-------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Route::get('/not-found', 'CommonController@not_found')->name('not-found');
Route::get('/server-error', 'CommonController@errorPage')->name('server.error');
//Backend//
Route::group(['prefix' => 'admin', 'namespace' => 'Backend'], function () {
    Route::get('/login', 'AuthController@login')->name('login');
    Route::post('/login/process', 'AuthController@processLogin')->name('process.login');
    Route::get('/logout', 'AuthController@logout')->name('logout');

    //ForgotPassword
    Route::get('/password/reset', 'ForgotPasswordController@showLinkRequestForm')->name('rest.form');
    Route::post('/password/email', 'ForgotPasswordController@sendResetLinkEmail')->name('reset.email');
    Route::get('/password/forgot/{token}', 'ForgotPasswordController@showResetForm')->name('reset.token');
    Route::post('/password/forgot/{token}', 'ForgotPasswordController@reset')->name('backend.reset');

    Route::group(['middleware' => ['admin.auth']], function () {
        Route::group(['middleware' => 'admin'], function () {
            Route::get('/', 'HomeController@index')->name('home');

            //product route
            Route::get('/products/list', 'ProductController@index')->name('products.index');
            Route::get('/products/create', 'ProductController@create')->name('products.create');
            Route::post('/products/store', 'ProductController@store')->name('products.store');
            Route::get('/products/details/{id}', 'ProductController@details')->name('products.details');
            Route::get('/products/edit/{id}', 'ProductController@edit')->name('products.edit');
            Route::post('/products/update/{id}', 'ProductController@update')->name('products.update');
            Route::post('/products/update/image/{id}', 'ProductController@updateImage')->name('products.image.upload');

            //category route
            Route::get('/categories', 'CategoryController@index')->name('categories.index');
            Route::post('/categories/store', 'CategoryController@store')->name('categories.store');
            Route::get('/categories/edit/{id}', 'CategoryController@edit')->name('categories.edit');
            Route::post('/categories/update/{id}', 'CategoryController@update')->name('categories.update');

            //Blog route
            Route::get('/blog', 'BlogController@index')->name('blog.index');
            Route::get('/blog/create', 'BlogController@create')->name('blog.create');
            Route::post('/blog/store', 'BlogController@store')->name('blog.store');
            Route::get('/blog/edit/{id}', 'BlogController@edit')->name('blog.edit');
            Route::post('/blog/update/{id}', 'BlogController@update')->name('blog.update');
            Route::get('/blog/delete/{id}', 'BlogController@delete')->name('blog.delete');


            //Slider route
            Route::get('/sliders/list', 'SliderController@index')->name('sliders.index');
            Route::post('/sliders/store', 'SliderController@store')->name('sliders.store');
            Route::get('/sliders/edit/{id}', 'SliderController@edit')->name('sliders.edit');
            Route::post('/sliders/update/{id}', 'SliderController@update')->name('sliders.update');
            Route::get('/sliders/delete/{id}', 'SliderController@delete')->name('sliders.delete');


            //Brand route
            Route::get('/brands/list', 'BrandController@index')->name('brands.index');
            Route::post('/brands/store', 'BrandController@store')->name('brands.store');
            Route::get('/brands/edit/{id}', 'BrandController@edit')->name('brands.edit');
            Route::post('/brands/update/{id}', 'BrandController@update')->name('brands.update');

            //NewsLetter route
            Route::get('/newsLetters', 'NewsLetterController@index')->name('newsLetters.index');
            Route::get('/newsLetters/delete/{id}', 'NewsLetterController@delete')->name('newsLetters.delete');

            //Employee route
            Route::get('/employees/list', 'UserController@index')->name('employees.index');
            Route::get('/employees/create', 'UserController@create')->name('employees.create');
            Route::post('/employees/store', 'UserController@store')->name('employees.store');
            Route::get('/employees/edit/{id}', 'UserController@edit')->name('employees.edit');
            Route::post('/employees/update/{id}', 'UserController@update')->name('employees.update');

            //customer route
            Route::get('/customers/list', 'CustomerController@index')->name('customers.index');
            Route::get('/customers/delete/{id}', 'CustomerController@delete')->name('customers.delete');


            //BusinessSetting route
            Route::get('/businessSettings/edit/{id}', 'BusinessSettingController@edit')->name('businessSettings.edit');
            Route::post('/businessSettings/update/{id}', 'BusinessSettingController@update')->name('businessSettings.update');


            //Order route
            Route::get('/orders/list/{status?}', 'OrderController@index')->name('orders.index');
            Route::get('/orders/details/{id}', 'OrderController@orderDetails')->name('orders.details');
            Route::get('/orders/invoice/{id}', 'OrderController@invoice')->name('orders.invoice');
            Route::get('/orders/status/{id}/{status}', 'OrderController@status')->name('orders.status');


            //Stock route
            Route::get('/stocks', 'StockController@index')->name('stocks.index');
            Route::get('/stocks/create', 'StockController@create')->name('stocks.create');
            Route::post('/stocks/store', 'StockController@store')->name('stocks.store');
            Route::get('/stocks/edit/{id}', 'StockController@edit')->name('stocks.edit');
            Route::post('/stocks/update/{id}', 'StockController@update')->name('stocks.update');

            //attribute //
            Route::get('/attribute', 'AttributeController@index')->name('attribute.index');
            Route::post('/attribute/store', 'AttributeController@store')->name('attribute.store');
            Route::get('/attribute/edit/{id}', 'AttributeController@edit')->name('attribute.edit');
            Route::post('/attribute/update/{id}', 'AttributeController@update')->name('attribute.update');


            //color //
            Route::get('/color', 'ColorController@index')->name('color.index');
            Route::post('/color/store', 'ColorController@store')->name('color.store');
            Route::get('/color/edit/{id}', 'ColorController@edit')->name('color.edit');
            Route::post('/color/update/{id}', 'ColorController@update')->name('color.update');


            //department //
            Route::get('/department/list', 'DepartmentController@index')->name('department.index');
            Route::post('/department/store', 'DepartmentController@store')->name('department.store');
            Route::get('/department/edit/{id}', 'DepartmentController@edit')->name('department.edit');
            Route::post('/department/update/{id}', 'DepartmentController@update')->name('department.update');

        });

    });
    });



Route::group(['namespace' => 'Frontend'], function () {
    Route::get('/', 'HomeController@index')->name('home.index');

    //ForgotPassword
    Route::get('/forgot/password', 'ForgotPasswordController@forgot')->name('forgot');
    Route::post('/forgot/password/reset', 'ForgotPasswordController@password')->name('password');
    Route::get('/password/reset/{token}', 'ForgotPasswordController@showResetForm')->name('password.token');
    Route::post('/password/reset/{token}', 'ForgotPasswordController@reset')->name('frontend.reset');


//    Cart //
    Route::get('/cart/index', 'CartController@index')->name('cart.index');
    Route::get('/cart/addToCart/{id}', 'CartController@addToCart')->name('add.cart');
    Route::post('/carts/update', 'CartController@updateCart')->name('update.cart');
    Route::get('/carts/delete/{id}', 'CartController@delete')->name('delete.cart');
    Route::get('/carts/destroy', 'CartController@destroy')->name('destroy.cart');


    // Auth //
    Route::get('/registration', 'AuthController@index')->name('registration');
    Route::post('/registration/create', 'AuthController@create')->name('registration.add');
    Route::post('/processLogin', 'AuthController@processLogin')->name('login.add');

    //Product //
    Route::get('/product/index', 'ProductController@index')->name('product.index');
    Route::get('/product/details/{id}', 'ProductController@details')->name('product.details');
    Route::get('/product/categoryWiseProduct/{id}', 'CategoryController@categoryWiseProduct')->name('categoryWiseProduct');
    Route::get('/category/index', 'CategoryController@index')->name('category.index');


    Route::group(['middleware' => 'auth'], function () {

        Route::get('/logout/home', 'AuthController@logout')->name('logout.home');
        //wishlist
        Route::get('/wishlist', 'WishlistController@index')->name('wishlist.index');
        Route::get('/wishlist/addToWishlist/{id}', 'WishlistController@addToWishlist')->name('add.wishlist');
        Route::get('/wishlist/delete/{id}', 'WishlistController@delete')->name('wishlist.delete');


        //profile
        Route::get('/customer-profiles/', 'UserController@profile')->name('customer.account');
        Route::get('/customer-profiles/edit/{id}', 'UserController@edit')->name('customer.profile.edit');
        Route::post('/customer-profiles/update/{id}', 'UserController@update')->name('customer.profile.update');

        //Newsletter
        Route::post('/newsLetter', 'NewsLetterController@create')->name('newsLetter.create');

        //Checkout//
        Route::get('/checkout/index', 'OrderController@index')->name('checkout.index');
        Route::post('/checkout/index', 'OrderController@create')->name('checkout.create');
        Route::get('/checkout/details/{id}', 'UserController@details')->name('checkout.details');

    });


});

